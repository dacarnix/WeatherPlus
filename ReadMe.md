Weather Plus is an overlay to the Google Maps V2 API for android, which allows users to save locations (via markers), as well as track the users current location. The application will take extensive checks to ensure that there is a stable network connection as well as having location permissions required for total functionality of the app. If there is no connection,
or weak permissions, then limited functionality will ensue and the user will be made away of this.

The application itself is built so that a user can save a list of locations all around the world, and also easily see current weather data at each location. At all times, assuming appropriate permissions and a valid internet connection exists, the user may see their
current weather information at their current location in the navigational drawer in the main page.

Users can add locations by tapping on the map, by searching for a location, or by going to current location. This will create a blue marker on the map (distinct from the red ones which signify already saved locations). When here, a floating action button will appear which if clicked, will allow the user to save this location. If the user taps on an already saved location, the floating action button will change to a "search" icon, which if clicked, will go open the saved locations view filtered for that marker.

Locations themselves (once saved) can be viewed, in a list (from the navigational drawer) and then seen in more detail, shared to others (via any application which accepts raw text, in the form of LAT/LONG coords) or to delete the saved location. 

While on the screen showing the saved locations, user may search for attributes in here using the searchbar in the toolbar. Searching by keywords such as country, nickname or type of location.

Users may see there saved locations on the world map, as well as filter what types of markers they see (the type is determined by the user when they save the location) via the settings fragment, toggled by the "cog" in the toolbar.

Users may also search for locations using the searchbar in the toolbar, which utilizes googles best-matched address to attempt to find what they are searching for. Also in this fashion, Weather Plus will accept raw-text input (from other applications) which will be passed in to Googles best-address match also.

The users current location information can therefore be used to mark there location (and add it as a saved location), and always at all times (if permissions allow, and a network connection) will show their current weather in the navigational drawer.