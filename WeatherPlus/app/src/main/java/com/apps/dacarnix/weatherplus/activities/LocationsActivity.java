package com.apps.dacarnix.weatherplus.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.apps.dacarnix.weatherplus.R;
import com.apps.dacarnix.weatherplus.database.repo.LocationRepo;
import com.apps.dacarnix.weatherplus.database.repo.WeatherRepo;
import com.apps.dacarnix.weatherplus.fragments.LocationListFragment;
import com.apps.dacarnix.weatherplus.fragments.SeeLocationFragment;
import com.apps.dacarnix.weatherplus.helpers.LocationActivityAlertBuilderHelper;
import com.apps.dacarnix.weatherplus.helpers.MapActivityConstants;
import com.apps.dacarnix.weatherplus.models.Location;
import com.apps.dacarnix.weatherplus.models.recyclerview.SavedLocationItemClickAction;

/**
 * An activity which shows the saved locations in a recyclerview to the user
 * The saved locations are loaded via the SQLite database, and will show more activity
 * in a second fragment if the screen width is large enough, else will open a new activity.
 * <p>
 * This inherits off of the location list fragment listener, to retrieve data and also
 * the searchview to include the search functionality in the toolbar.
 * <p>
 * Clicking on an item will intent to a new activity to show more indepth information regarding
 * a location, ALONG with its weather data. Since weather data is ensured to be up-to-date during
 * the mapactivity lifecycle (ie before it gets here) - unless there is a network issue - then we can
 * simply pull the weather data from the database for the given location.
 * <p>
 * If the screen width is large enough, this will go in to a master detail rather than opening a new activity.
 */
public class LocationsActivity extends AppCompatActivity implements LocationListFragment.LocationListFragmentListener, SearchView.OnQueryTextListener {

    public static final String EXTRA_QUERY = "com.apps.dacarnix.weatherplus.EXTRA_QUERY";

    /**
     * Attempts to launch this activity based on another
     * This takes in information of the marker name clicked on
     *
     * @param activity the activity to launch from
     * @param title    the title of the marker selected
     */
    public static void launchActivity(Activity activity, String title) {
        Intent intent = new Intent(activity, LocationsActivity.class);
        if (title != null) {
            intent.putExtra(LocationsActivity.EXTRA_QUERY, title);
        }
        activity.startActivityForResult(intent, MapActivityConstants.UNIQUE_CODE_LOCATION);
    }

    @Nullable
    private LocationListFragment mLocationFragment;

    @Nullable
    private String mQuery;

    @Nullable
    private SeeLocationFragment mSeeLocationFragment;

    /**
     * Set up the activity and grab a references
     * to the UI components involved.
     *
     * @param savedInstanceState contains potentially persistent data
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locations);
        View fragmentView = findViewById(R.id.fragment_location_list);
        if (fragmentView != null) {
            FragmentManager fm = getSupportFragmentManager();
            mLocationFragment = (LocationListFragment) fm.findFragmentById(R.id.fragment_location_list);
        }
        setToolbar();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mQuery = bundle.getString(EXTRA_QUERY);
        }

        fragmentView = findViewById(R.id.fragment_see_location);
        // Checks to see if this is a master detail fragment and if so, there will exist another
        // view with this fragment.
        if (fragmentView != null) {
            FragmentManager fm = getSupportFragmentManager();
            mSeeLocationFragment = (SeeLocationFragment) fm.findFragmentById(R.id.fragment_see_location);
            fragmentView.setVisibility(View.INVISIBLE);
        } else {
            mSeeLocationFragment = null;
        }

    }

    /**
     * Allows the user to click on the back button to return to the main menu
     *
     * @param menuItem the item pressed
     * @return a boolean regarding if the event has been handled.
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    /**
     * Sets up the menu in the tool bar.
     * This also provides a search bar in which users can filter down
     * on the saved locations.
     *
     * @param menu the menu object to inflate
     * @return true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_locations, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        mSearchView.setQueryHint(getString(R.string.searchbar_saved_locations_hint));
        mSearchView.setOnQueryTextListener(this);
        if (mQuery != null) {
            mSearchView.setQuery(mQuery, true);
        }
        return true;
    }

    /**
     * Handle appropriately what to do when the user preforms an action
     * with a particular location entry.
     *
     * @param location the location data
     * @param action   the action to preform.
     */
    @Override
    public void onLocationClick(Location location, SavedLocationItemClickAction action) {
        switch (action) {
            case Share:
                // Share location data with an open broadcast
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_TEXT, location.getShareableLocation());
                shareIntent.setType("text/plain");
                startActivity(Intent.createChooser(shareIntent,
                        getResources().getText(R.string.msg_share_location_data)));
                break;
            case Delete:
                deleteLocation(location.getId());
                break;
            case GoToWeather:
                // If this is on a master detail, update the fragment, else intent to the new activity
                if (mSeeLocationFragment == null) {
                    SeeLocationActivity.launchActivity(this, location);
                } else {
                    findViewById(R.id.fragment_see_location).setVisibility(View.VISIBLE);
                    mSeeLocationFragment.loadContents(location,
                            WeatherRepo.getWeatherForLocation(location.getId()), true);
                }
                break;
        }
    }

    /**
     * When the user changes the query string in the toolbar, update it
     * and refresh the cursor appropriately such that the list
     * reflects the query.
     *
     * @param query the entered search bar query.
     * @return true
     */
    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mQuery = newText;
        if (newText.isEmpty()) {
            mQuery = null;
        }
        mLocationFragment.updateCursor(mQuery);
        return false;
    }

    /**
     * Sets up the tool bar
     */
    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar action = getSupportActionBar();
        if (action != null) {
            action.setTitle("");
            action.setDisplayHomeAsUpEnabled(true);
            action.setHomeButtonEnabled(true);
        }
    }

    /**
     * Confirms the user wanted to delete the current location entry
     * via a dialog popup. If they do, delete it from the database
     * and refresh the recyclerview.
     *
     * @param uniqueId the unique ID to delete
     */
    private void deleteLocation(final int uniqueId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.title_delete_confirmation_dialog);
        builder.setMessage(R.string.msg_delete_confirmation_dialog);
        builder.setPositiveButton(R.string.action_delete_confirmation_dialog, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                LocationRepo rep = new LocationRepo();
                rep.deleteRecord(uniqueId);
                mLocationFragment.updateCursor(mQuery);
                Toast.makeText(LocationsActivity.this, R.string.msg_delete_record_toast, Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton(R.string.action_dont_delete_dialog, null);
        builder.create().show();
    }

    /**
     * Show the help dialog
     *
     * @param item the help dialog clicked on
     */
    public void menuHelpClick(MenuItem item) {
        LocationActivityAlertBuilderHelper.getHelpDialog(this).show();
    }
}
