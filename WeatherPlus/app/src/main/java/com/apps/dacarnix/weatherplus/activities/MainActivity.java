package com.apps.dacarnix.weatherplus.activities;

import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.apps.dacarnix.weatherplus.R;
import com.apps.dacarnix.weatherplus.fragments.MapPreferenceFragment;
import com.apps.dacarnix.weatherplus.fragments.MyMapFragment;
import com.apps.dacarnix.weatherplus.fragments.NavigatorDrawerFragment;
import com.apps.dacarnix.weatherplus.helpers.MapActivityAlertBuilderSingleton;
import com.apps.dacarnix.weatherplus.helpers.MapActivityUIHelper;
import com.apps.dacarnix.weatherplus.helpers.MapActivityConstants;
import com.apps.dacarnix.weatherplus.helpers.MapActivityMapsHelper;
import com.apps.dacarnix.weatherplus.models.NetworkStateReceiver;
import com.apps.dacarnix.weatherplus.viewmodels.WeatherAndLocationsViewModel;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

/**
 * Map Activity is the entry point of this program and inherits many listeners.
 * It involves a drawer navigator and settings panel, and revolves around google maps
 * as the core functionality. Users can add markers and save them here.
 * This will also ensure that location permissions and networking connections are set up.
 */
public class MainActivity extends AppCompatActivity implements
        View.OnClickListener,
        NavigatorDrawerFragment.NavigatorDrawerFragmentListener,
        SearchView.OnQueryTextListener,
        MapPreferenceFragment.PreferenceChangeListener,
        MyMapFragment.LocationPermissionsListener,
        NetworkStateReceiver.NetworkStateReceiverListener {

    // Map related variables
    private MenuItem mSearchItem;
    // UI related variables
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private View mSettingsFragmentContainer;
    private MapActivityUIHelper mAnimationHelper = new MapActivityUIHelper();
    private FloatingActionButton mLocationAction;
    private NavigationView mNavView;
    // View models and fragments
    private WeatherAndLocationsViewModel mWeatherAndLocationsViewModel;
    private MyMapFragment mMapFragment;

    // This will only ever be true if settings were open on rotation
    private boolean mInstantSettingsOpen = false;

    private Snackbar mInternetSnackbar;
    private NetworkStateReceiver mNetworkListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpNavigator();
        setToolBar();
        setUpFab();
        mWeatherAndLocationsViewModel = ViewModelProviders.of(this).get(WeatherAndLocationsViewModel.class);
        mMapFragment = (MyMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_fragment);
        if (savedInstanceState != null) {
            mAnimationHelper.updateNavigatorDrawerHeader(this, mNavView,
                    mWeatherAndLocationsViewModel.getCurrentWeather());
            mInstantSettingsOpen = savedInstanceState.getBoolean(MapActivityConstants.BUNDLE_SETTINGS_ACTIVE);
        }

        mInternetSnackbar = Snackbar.make(findViewById(R.id.coordinator_layout),
                R.string.msg_no_internet, Snackbar.LENGTH_INDEFINITE);
    }


    /**
     * Is called when a network becomes available and sets the appropriate UI components to view
     */
    @Override
    public void onNetworkAvailable() {
        boolean wasDisabled = mInternetSnackbar.isShown();
        mInternetSnackbar.dismiss();
        if (wasDisabled) {
            Snackbar.make(findViewById(R.id.coordinator_layout), R.string.msg_internet_back,
                    Snackbar.LENGTH_SHORT).show();
        }
        if (!mAnimationHelper.getSettingsOpen() && mSearchItem != null) {
            mSearchItem.getActionView().setVisibility(View.VISIBLE);
            mSearchItem.setVisible(true);
        }
    }

    /**
     * Hide the appropriate UI actions that require internet access
     */
    @Override
    public void onNetworkUnavailable() {
        mInternetSnackbar.show();
        if (mSearchItem != null) {
            mSearchItem.getActionView().setVisibility(View.GONE);
            mSearchItem.setVisible(false);
        }
        if (mAnimationHelper.isAddFab()) mLocationAction.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(MapActivityConstants.BUNDLE_SETTINGS_ACTIVE, mAnimationHelper.getSettingsOpen());
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                checkCurrentWeather();
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.menu_settings:
                toggleSettings(false);
                return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    /**
     * Checks if there is current weather data to put in to the drawer header
     */
    private void checkCurrentWeather() {
        mAnimationHelper.updateNavigatorDrawerHeader(this, mNavView,
                mWeatherAndLocationsViewModel.getCurrentWeather());
    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        mDrawerToggle.syncState();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mWeatherAndLocationsViewModel.checkChanges(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mNetworkListener = new NetworkStateReceiver(this);
        mNetworkListener.addListener(this);
        this.registerReceiver(mNetworkListener, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mNetworkListener);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        // Permissions are approved!
        if (requestCode == MapActivityConstants.PERMISSIONS_FINE_LOCATION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mMapFragment.myLocationRequest();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_map, menu);
        mSearchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) mSearchItem.getActionView();
        searchView.setQueryHint(getString(R.string.searchbar_maps_hint));
        searchView.setOnQueryTextListener(this);
        checkIntent();

        // Once the menu has loaded, then toggle the settings if they were open on rotation
        if (mInstantSettingsOpen) {
            toggleSettings(true);
            mInstantSettingsOpen = false;
        }

        return true;
    }


    @Override
    public void onEntryClick(int uniqueResource) {
        switch (uniqueResource) {
            case R.drawable.ic_info_black_24dp:
                MapActivityAlertBuilderSingleton.getInstance().getAboutDialog(this);
                break;
            case R.drawable.ic_location_city_black_24dp:
                LocationsActivity.launchActivity(this, null);
                break;
        }
    }

    /**
     * When the user submits a query, try find the appropriate "best address" via the google maps API
     *
     * @param query the query the user typed in
     * @return the event has been handled.
     */
    @Override
    public boolean onQueryTextSubmit(String query) {
        LatLng location = MapActivityMapsHelper.getBestAddress(this, query);
        if (location != null) {
            mMapFragment.goToLocation(location, MapActivityConstants.DEFAULT_ZOOM_LEVEL, false);
        } else {
            Toast.makeText(this, R.string.msg_failure_location_finder, Toast.LENGTH_SHORT).show();
        }
        mSearchItem.getActionView().clearFocus();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    /**
     * When a preference has been changed in the settings fragment, update the locations shown
     */
    @Override
    public void onPreferenceChange() {
        mWeatherAndLocationsViewModel.checkChanges(this);
        mMapFragment.refreshSavedMarkers();
    }

    /**
     * On success of adding a location, turn the blue marker in to a red (saved) marker
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MapActivityConstants.UNIQUE_CODE_CREATE_LOCATION && resultCode == Activity.RESULT_OK) {
            mMapFragment.savedCurrentMarker();
        } else if (requestCode == MapActivityConstants.UNIQUE_CODE_LOCATION) {
            // If a location was deleted, and it was potentially selected (the marker)
            // on the map, deselect any markers.
            mWeatherAndLocationsViewModel.refreshQuery();
            if (mWeatherAndLocationsViewModel.deletionOccured()) {
                mMapFragment.clearSelectedMarkers();
                // Also need to re-hide the action FAB
                mLocationAction.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public void onMarkerClick(boolean savedLocation) {
        mAnimationHelper.animateLocationFabButton(mLocationAction,
                savedLocation, mNetworkListener.isConnected());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.settings_preference_holder:
                break;
            case R.id.fab_action_location:
                Marker tapMarker = mMapFragment.getOnTapMarker();
                if (tapMarker != null) {
                    if (!mMapFragment.selectedSavedMarker()) {
                        AddLocationActivity.launchActivity(this, tapMarker.getPosition());
                    } else {
                        LocationsActivity.launchActivity(this, tapMarker.getTitle());
                    }
                }
                break;
            case R.id.fab_my_location:
                mMapFragment.goToLocation(null, MapActivityConstants.DEFAULT_ZOOM_LEVEL, true);
                break;
        }
    }

    private void setUpNavigator() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_map);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        mNavView = (NavigationView) findViewById(R.id.nav_view);
    }

    /**
     * Sets up the tool bar on the main activitys UI
     */
    private void setToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar action = getSupportActionBar();
        if (action != null) {
            action.setTitle("");
            action.setDisplayHomeAsUpEnabled(true);
            action.setHomeButtonEnabled(true);
            action.setHomeAsUpIndicator(R.drawable.ic_list_black_24dp);
        }

        mSettingsFragmentContainer = findViewById(R.id.settings_preference_holder);
        mSettingsFragmentContainer.setOnClickListener(this);
        getFragmentManager().beginTransaction().
                replace(R.id.cardview_preference_container, new MapPreferenceFragment())
                .commit();
    }


    private void setUpFab() {
        mLocationAction = (FloatingActionButton) findViewById(R.id.fab_action_location);
        FloatingActionButton mMyLocation = (FloatingActionButton) findViewById(R.id.fab_my_location);
        mLocationAction.setOnClickListener(this);
        mMyLocation.setOnClickListener(this);
    }

    /**
     * Checks incase this activity was launched from another application with data
     */
    private void checkIntent() {
        String intentString = MapActivityMapsHelper.getIntentString(getIntent());
        // If there is no internet connection, prompt the user regarding the issue
        if (intentString != null && !mNetworkListener.isConnected()) {
            mSearchItem.getActionView().setVisibility(View.GONE);
            mSearchItem.setVisible(false);
            Toast.makeText(this, R.string.msg_error_search, Toast.LENGTH_LONG).show();
        } else if (intentString != null) {
            ((SearchView) mSearchItem.getActionView()).setQuery(intentString, true);
        }
    }

    /**
     * Open or shut the settings
     *
     * @param instant whether to open the settings instantly or not in the animation
     */
    private void toggleSettings(boolean instant) {
        mAnimationHelper.animateSettingsFragmentMapActivity(mSettingsFragmentContainer,
                mSearchItem, -mSettingsFragmentContainer.getHeight(),
                getSupportActionBar().getHeight(), instant, mNetworkListener.isConnected());
    }

    /**
     * Ensures location positions are acquired for full function.
     * If not, prompts the user to change.
     *
     * @return true if permissions are acquired
     */
    public boolean checkLocationPermission() {
        if (!mMapFragment.isLocationServicesEnabled()) {
            MapActivityAlertBuilderSingleton.getInstance().getLocationServicesOnDialog(this);
        } else if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // If we should show an explanation
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                MapActivityAlertBuilderSingleton.getInstance().getFineLocationPermissionDialog(this);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MapActivityConstants.PERMISSIONS_FINE_LOCATION_CODE);
            }
            return false;
        } else {
            return true;
        }
        return false;
    }
}
