package com.apps.dacarnix.weatherplus.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.apps.dacarnix.weatherplus.helpers.MapActivityConstants;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * A basic class to hold a location (in long-lat), its
 * name identifier (such as School) and its type of location (ie friends house, public building).
 * It will also hold additional information useful for database operations such
 * as a unique identifier (for deletion / lookup) and time of creation.
 * <p>
 * Implements parcelable to allow the passing to activites in intents
 */

public class Location implements Parcelable {

    private static NumberFormat sLatLongFormatter = new DecimalFormat("#0.00");

    @Nullable
    private String mType;
    private double mLongitude;
    private double mLatitude;
    // Stored as UNIX time
    @Nullable
    private Long mTimeOfEntry;
    @Nullable
    private String mName;
    @Nullable
    private String mCountry;
    private int mId;
    // Used to hold an icon related to the type defined in mType
    private int mImageIcon;

    public Location(@Nullable String type,
                    @Nullable String name,
                    double longitude,
                    double latitude,
                    @Nullable String country,
                    @Nullable Long time) {
        mType = type;
        mName = name;
        mLongitude = longitude;
        mLatitude = latitude;
        mTimeOfEntry = time;
        mCountry = country;
    }

    /**
     * Special constructor for parcels.
     * @param in the parcel object to convert back to a location
     */
    protected Location(Parcel in) {
        mType = in.readString();
        mLongitude = in.readDouble();
        mLatitude = in.readDouble();
        mName = in.readString();
        mCountry = in.readString();
        mId = in.readInt();
        mImageIcon = in.readInt();
    }

    public static final Creator<Location> CREATOR = new Creator<Location>() {
        @Override
        public Location createFromParcel(Parcel in) {
            return new Location(in);
        }

        @Override
        public Location[] newArray(int size) {
            return new Location[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mType);
        dest.writeDouble(mLongitude);
        dest.writeDouble(mLatitude);
        dest.writeString(mName);
        dest.writeString(mCountry);
        dest.writeInt(mId);
        dest.writeInt(mImageIcon);
    }

    public String getName() {
        return mName;
    }

    public String getType() {
        return mType;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public String getLongFormatted() {
        return sLatLongFormatter.format(mLongitude);
    }

    public String getLatFormatted() {
        return sLatLongFormatter.format(mLatitude);
    }

    public Long getTimeStamp() {
        return mTimeOfEntry;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getId() {
        return mId;
    }

    public void setImageIcon(int icon) {
        mImageIcon = icon;
    }

    public int getImageIcon() {
        return mImageIcon;
    }

    /**
     * This returns the string that will be used to share
     * with other applicables regarding a location
     */
    public String getShareableLocation() {
        return String.format(MapActivityConstants.LOCATION_SHARING_PREFIX, mLatitude, mLongitude);
    }
}
