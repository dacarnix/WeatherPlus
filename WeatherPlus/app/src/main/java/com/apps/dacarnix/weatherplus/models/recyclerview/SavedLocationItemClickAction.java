package com.apps.dacarnix.weatherplus.models.recyclerview;

/**
 * A basic enum referring to which action the on-click referred to.
 */
public enum SavedLocationItemClickAction {
    GoToWeather,
    Share,
    Delete
}
