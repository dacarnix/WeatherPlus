package com.apps.dacarnix.weatherplus.database;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

/**
 * Manages the usage of the database over the lifecycle of the application.
 * Provides access to the database.
 * <p>
 * Follows the singleton pattern.
 */
public class DBManager {
    @Nullable
    private Integer mOpenCounter = 0;
    @Nullable
    private static DBManager instance;
    @Nullable
    private static SQLiteOpenHelper mDatabaseHelper;
    @Nullable
    private SQLiteDatabase mDatabase;

    public static synchronized void initializeInstance(SQLiteOpenHelper helper) {
        if (instance == null) {
            instance = new DBManager();
            mDatabaseHelper = helper;
        }
    }

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            throw new IllegalStateException("Class is not initialized");
        }
        return instance;
    }

    /**
     * Keeps track of how many open instances there are.
     * @return an instance of the database for access
     */
    public synchronized SQLiteDatabase openDatabase() {
        mOpenCounter += 1;
        if (mOpenCounter == 1) {
            mDatabase = mDatabaseHelper.getWritableDatabase();
        }
        return mDatabase;
    }

    /**
     * Signals the close of a database.
     * If all are closed, then it will shut down
     * until another is opened again.
     */
    public synchronized void closeDatabase() {
        mOpenCounter -= 1;
        if (mOpenCounter == 0) {
            mDatabase.close();
        }
    }
}