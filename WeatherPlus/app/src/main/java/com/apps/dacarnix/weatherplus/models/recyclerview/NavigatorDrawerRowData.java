package com.apps.dacarnix.weatherplus.models.recyclerview;

/**
 * A class which holds the data associated with each row
 * in the navigator drawer in the main activity
 */

public class NavigatorDrawerRowData {
    private String mItemDescription;
    private Integer mImageTypeId;

    public NavigatorDrawerRowData(String description, Integer id) {
        mItemDescription = description;
        mImageTypeId = id;
    }

    public String getDescription() {
        return mItemDescription;
    }

    public Integer getImageId() {
        return mImageTypeId;
    }
}
