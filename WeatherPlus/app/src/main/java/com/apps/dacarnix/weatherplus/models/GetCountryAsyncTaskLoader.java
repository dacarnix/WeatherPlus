package com.apps.dacarnix.weatherplus.models;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.content.AsyncTaskLoader;

import com.apps.dacarnix.weatherplus.helpers.MapActivityConstants;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;

/**
 * A task which given a lat-long position will attempt to find the corresponding country
 * in an ASYNC fashion to avoid blocking the UI thread.
 * <p>
 * Uses AsyncTaskLoader opposed to AsyncTask to help better handle orientation changes
 * <p>
 * This is used exclusively in AddLocationActivity.
 */

public class GetCountryAsyncTaskLoader extends AsyncTaskLoader<String> {

    private Geocoder mGeocoder;
    private LatLng mLocation;

    public GetCountryAsyncTaskLoader(Context context, LatLng location) {
        super(context);
        mGeocoder = new Geocoder(context);
        mLocation = location;
    }

    /**
     * Attempts to get the country based on the lat and long.
     *
     * @return the country, if none was found, it will return the default value.
     */
    @Override
    public String loadInBackground() {
        List<Address> addressList;
        try {
            addressList = mGeocoder.getFromLocation(mLocation.latitude, mLocation.longitude, 1);
            if (addressList.size() > 0) {
                return addressList.get(0).getCountryName();
            }
        } catch (IOException e) {
            return MapActivityConstants.DEFAULT_COUNTRY;
        }
        return MapActivityConstants.DEFAULT_COUNTRY;
    }
}
