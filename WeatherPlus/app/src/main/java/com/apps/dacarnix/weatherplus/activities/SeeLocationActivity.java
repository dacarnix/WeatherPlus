package com.apps.dacarnix.weatherplus.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.apps.dacarnix.weatherplus.R;
import com.apps.dacarnix.weatherplus.database.repo.WeatherRepo;
import com.apps.dacarnix.weatherplus.fragments.SeeLocationFragment;
import com.apps.dacarnix.weatherplus.models.Location;

/**
 * An activity which shows more in-depth data regarding a saved location
 * Launched only from the LocationsActivity by clicking on a saved location
 * in the recyclerview.
 */
public class SeeLocationActivity extends AppCompatActivity {

    public static final String EXTRA_LOCATION = "com.apps.dacarnix.weatherplus.EXTRA_LOCATION";
    private Location mLocation;

    /**
     * Launches this activity with the location in the bundle
     *
     * @param activity the activity to launch from
     * @param location what to add in the bundle
     */
    public static void launchActivity(Activity activity, Location location) {
        Intent intent = new Intent(activity, SeeLocationActivity.class);
        intent.putExtra(EXTRA_LOCATION, location);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_location);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            mLocation = bundle.getParcelable(EXTRA_LOCATION);
        }

        ActionBar actionBar = getSupportActionBar();
        // Enable the back button in the tool bar
        if (actionBar != null) {
            actionBar.setTitle("");
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        SeeLocationFragment seeFrag = (SeeLocationFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_see_location);
        seeFrag.loadContents(mLocation, WeatherRepo.getWeatherForLocation(mLocation.getId()), false);
    }


    /**
     * Allows the user to click on the back button to return to the main menu
     *
     * @param menuItem the item pressed
     * @return a boolean regarding if the event has been handled.
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

}
