package com.apps.dacarnix.weatherplus.helpers;


import android.support.annotation.Nullable;

import com.apps.dacarnix.weatherplus.R;
import com.apps.dacarnix.weatherplus.app.App;

import java.util.HashMap;
import java.util.Map;

/**
 * A static class which holds methods to help with the location types.
 * This includes getting a relevant icon for a given type.
 */
public class LocationTypeHelper {
    @Nullable
    private static Map<String, Integer> sTypeToIcon;
    private static int sDefaultIcon;
    @Nullable
    private static String[] sTypeNames;

    static {
        sDefaultIcon = R.drawable.ic_my_location_black_24dp;
        sTypeToIcon = new HashMap<>();

        // To match the types with icons, based on the type locations in arrays.xml.
        // We ignore the "all" type.
        int[] imageIds = new int[]{R.drawable.ic_location_city_black_24dp,
                R.drawable.ic_person_black_24dp,
                R.drawable.ic_school_black_24dp,
                R.drawable.ic_nature_people_black_24dp,
                sDefaultIcon};

        sTypeNames = App.getContext().
                getResources().
                getStringArray(R.array.location_types);

        for (int i = 0; i < sTypeNames.length; i++) {
            sTypeToIcon.put(sTypeNames[i], imageIds[i]);
        }
    }

    public static String[] getTypes() {
        return sTypeNames;
    }

    /**
     * Given the type of a location, find an appropriate icon
     *
     * @param type the type of the location
     * @return the icon for the type
     */
    public static int getIcon(String type) {
        return !sTypeToIcon.containsKey(type)
                ? sDefaultIcon
                : sTypeToIcon.get(type);
    }
}
