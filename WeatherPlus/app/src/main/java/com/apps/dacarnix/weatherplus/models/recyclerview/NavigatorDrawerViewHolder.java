package com.apps.dacarnix.weatherplus.models.recyclerview;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.dacarnix.weatherplus.R;

/**
 * The view holder of the rows in the navigational drwaer recyclerlist.
 * Handles the updating and displaying of each row.
 */
public class NavigatorDrawerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final NavigatorItemClickListener mListener;
    @NonNull
    private final ImageView mImageType;
    @NonNull
    private final TextView mTextDescription;

    /**
     * Gets references to the UI components contained in the list, and sets
     * up the appropriate click listeners
     *
     * @param itemView the view of the row
     * @param listener the listener to clicks
     */
    public NavigatorDrawerViewHolder(@NonNull View itemView, NavigatorItemClickListener listener) {
        super(itemView);
        mListener = listener;
        itemView.setOnClickListener(this);
        mImageType = (ImageView) itemView.findViewById(R.id.image_item_type);
        mTextDescription = (TextView) itemView.findViewById(R.id.text_item_description);
    }

    @Override
    public void onClick(View view) {
        int dataPosition = getAdapterPosition();
        mListener.onClick(dataPosition);
    }

    /**
     * Updates the UI components with the respective data
     *
     * @param data the data of the row to show
     */
    public void updateEntries(@NonNull NavigatorDrawerRowData data) {
        mTextDescription.setText(data.getDescription());
        mImageType.setBackgroundResource(data.getImageId());
    }

    /**
     * A simple interface to create a valid on-click event within the navigator recyclerview
     */
    public interface NavigatorItemClickListener {
        void onClick(int position);
    }
}
