package com.apps.dacarnix.weatherplus.database.repo;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;

import com.apps.dacarnix.weatherplus.database.DBHelper;
import com.apps.dacarnix.weatherplus.database.DBManager;
import com.apps.dacarnix.weatherplus.database.model.WeatherContract;
import com.apps.dacarnix.weatherplus.models.WeatherData;

/**
 * A class for handling the database operations surronding the hourly weather data tables.
 * Contains methods for retrieving, deleting and creating rows.
 */
public class WeatherRepo {

    /**
     * @return the SQL query for creating the table with the correct
     * column names and types for the saved weather data
     */
    public static String createTable() {
        return "CREATE TABLE " + WeatherContract.WeatherEntry.TABLE_NAME + "("
                + WeatherContract.WeatherEntry._ID + "   INTEGER PRIMARY KEY AUTOINCREMENT   ,"
                + WeatherContract.WeatherEntry.COLUMN_NAME_LOC_ID + " INT ,"
                + WeatherContract.WeatherEntry.COLUMN_NAME_TEMP + " REAL ,"
                + WeatherContract.WeatherEntry.COLUMN_NAME_HUMIDITY + " REAL ,"
                + WeatherContract.WeatherEntry.COLUMN_NAME_WEATHER_DESCRIPTION + " TEXT ,"
                + WeatherContract.WeatherEntry.COLUMN_NAME_WEATHER_ICON + " TEXT  ,"
                + WeatherContract.WeatherEntry.COLUMN_NAME_TIMESTAMP + " INT )";
    }

    /**
     * For a given location ID, return the appropriate weather data. This may be null
     *
     * @param locationId the location ID to map the weather data to
     * @return the weather data
     */
    @Nullable
    public static WeatherData getWeatherForLocation(Integer locationId) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();
        try (Cursor cursor = db.rawQuery(getQueryForId(), new String[]{locationId.toString()})) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                WeatherData data = fromCursor(cursor);
                data.setLocationId(locationId);
                return data;
            }
        }
        return null;
    }

    /**
     * Builds the raw query to find a weather row based on a specific location ID
     * Still requires a parameter
     *
     * @return the build string
     */
    private static String getQueryForId() {
        return "SELECT * FROM " + WeatherContract.WeatherEntry.TABLE_NAME
                + " WHERE " + WeatherContract.WeatherEntry.COLUMN_NAME_LOC_ID + "=?";
    }


    /**
     * Inserts weather data as a row based on the variable provided
     *
     * @param weather the weather to add
     * @return the ID of the added row
     */
    public int insert(WeatherData weather) {
        int weatherId;

        //  Return an error code
        if (weather.getLocationId() == null) return -1;

        // Since weather is a 1 to 1 relation with saved locations, delete any previous entries
        // Which if existed, would only have out-dated data
        deleteRecord(weather.getLocationId());

        SQLiteDatabase db = DBManager.getInstance().openDatabase();
        ContentValues values = new ContentValues();

        values.put(WeatherContract.WeatherEntry.COLUMN_NAME_LOC_ID, weather.getLocationId());
        values.put(WeatherContract.WeatherEntry.COLUMN_NAME_TEMP, weather.getTemperature());
        values.put(WeatherContract.WeatherEntry.COLUMN_NAME_HUMIDITY, weather.getHumidity());
        values.put(WeatherContract.WeatherEntry.COLUMN_NAME_WEATHER_DESCRIPTION, weather.getDescription());
        values.put(WeatherContract.WeatherEntry.COLUMN_NAME_WEATHER_ICON, weather.getIconTag());
        values.put(WeatherContract.WeatherEntry.COLUMN_NAME_TIMESTAMP, weather.getUnixTimeOfData());

        // Inserting Row
        weatherId = (int) db.insert(WeatherContract.WeatherEntry.TABLE_NAME, null, values);
        DBManager.getInstance().closeDatabase();

        return weatherId;
    }

    /**
     * Deletes a row based on their location ID
     *
     * @param id the unique location ID
     */
    public void deleteRecord(Integer id) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();
        db.delete(WeatherContract.WeatherEntry.TABLE_NAME,
                WeatherContract.WeatherEntry.COLUMN_NAME_LOC_ID + "=?",
                new String[]{id.toString()});
        DBManager.getInstance().closeDatabase();
    }

    /**
     * Loads a weather data object from the current row the cursor is pointing at
     * by extracting the column data.
     *
     * @param cursor the cursor object
     * @return the weather data based on the current cursor
     */
    public static WeatherData fromCursor(Cursor cursor) {
        WeatherData weather = new WeatherData(
                DBHelper.getColumnDouble(cursor, WeatherContract.WeatherEntry.COLUMN_NAME_TEMP),
                DBHelper.getColumnDouble(cursor, WeatherContract.WeatherEntry.COLUMN_NAME_HUMIDITY),
                DBHelper.getColumnData(cursor, WeatherContract.WeatherEntry.COLUMN_NAME_WEATHER_DESCRIPTION),
                DBHelper.getColumnData(cursor, WeatherContract.WeatherEntry.COLUMN_NAME_WEATHER_ICON),
                DBHelper.getColumnLong(cursor, WeatherContract.WeatherEntry.COLUMN_NAME_TIMESTAMP)
        );
        weather.setId(DBHelper.getColumnInt(cursor, WeatherContract.WeatherEntry._ID));
        return weather;
    }
}
