package com.apps.dacarnix.weatherplus.helpers;


import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;

import com.apps.dacarnix.weatherplus.R;
import com.apps.dacarnix.weatherplus.activities.MainActivity;

/**
 * A singleton class which holds data for alerts within the map activity.
 * It is a singleton opposed to a static class as it holds state variables
 * to ensure no dialog opens more than once.
 */
public class MapActivityAlertBuilderSingleton {

    private boolean mLocationServicesDialogOpen;
    private boolean mFineLocationDialogOpen;

    private static MapActivityAlertBuilderSingleton instance;

    public static MapActivityAlertBuilderSingleton getInstance() {
        if (instance == null) {
            instance = new MapActivityAlertBuilderSingleton();
        }
        return instance;
    }

    private MapActivityAlertBuilderSingleton() {
        mLocationServicesDialogOpen = false;
        mFineLocationDialogOpen = false;
    }

    /**
     * Returns a simple dialog used in the map activity.
     * Displays information to the user regarding the application.
     *
     * @param context required to create the activity
     */
    public void getAboutDialog(@NonNull Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.title_about_dialog);
        builder.setMessage(R.string.msg_about_dialog);
        builder.setPositiveButton(R.string.action_about_button, null);
        builder.create();
        builder.show();
    }

    /**
     * Creates a dialog explaining why we need the fine location position and
     * then re-prompts the user.
     * <p>
     * Enforces that only one dialog can open at a time
     *
     * @param context the map activity
     */
    public void getFineLocationPermissionDialog(@NonNull final MainActivity context) {
        if (!mFineLocationDialogOpen) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(R.string.title_location_dialog);
            builder.setMessage(R.string.msg_location_dialog);
            builder.setPositiveButton(R.string.action_location_dialog, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //Prompt the user once explanation has been shown
                    mFineLocationDialogOpen = false;
                    ActivityCompat.requestPermissions(context,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MapActivityConstants.PERMISSIONS_FINE_LOCATION_CODE);

                }
            });
            builder.setNegativeButton(R.string.action_fine_location_negative, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    mFineLocationDialogOpen = false;

                }
            });
            builder.create();
            mFineLocationDialogOpen = true;
            builder.show();
        }
    }

    /**
     * Displays to the user why they should enable location services.
     * Enforces that only one dialog will be open at a time
     */
    public void getLocationServicesOnDialog(@NonNull final Context context) {
        if (!mLocationServicesDialogOpen) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(R.string.title_enable_location_dialog);
            builder.setMessage(context.getString(R.string.msg_enable_location_dialog));
            builder.setPositiveButton(R.string.action_enable_location_positive, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    context.startActivity(myIntent);
                    mLocationServicesDialogOpen = false;
                }
            });
            builder.setNegativeButton(R.string.action_cancel_button, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    mLocationServicesDialogOpen = false;
                }
            });
            mLocationServicesDialogOpen = true;
            builder.show();
        }
    }
}

