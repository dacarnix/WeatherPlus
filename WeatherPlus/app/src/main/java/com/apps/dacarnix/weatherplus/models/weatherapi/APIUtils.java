package com.apps.dacarnix.weatherplus.models.weatherapi;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple class to hold constants and static helper methods related
 * to weather API calls
 */
public class APIUtils {
    // Api key for the FREE limited account for openweathermap
    private static final String API_KEY = "05159ed80dc5c47dda3cf17957e623ab";

    // Base URL for the weather API calls
    public static final String BASE_URL = "http://api.openweathermap.org/";

    // The api call string to get weather data for the hour. LAT, LONG, API KEY.
    public static final String WEATHER_HOURLY_API = "/data/2.5/weather";

    // The tag in the URL to specify the latitude
    public static final String LATITUDE_TAG = "lat";

    // The tag in the URL to specify the longitude
    public static final String LONGITUDE_TAG = "lon";

    // The tag in the URL to specify the application key
    public static final String APP_KEY_TAG = "appid";

    public static final Double KELVIN_TO_CELCIUS_OFFSET = -273.15;

    /**
     * Gets the API tags to use in the weather API hourly call, for a given
     * latitude and longitude.
     *
     * @param lon the longitude of the location
     * @param lat the latitude of the location
     * @return a map of tags to use in the api call
     */
    public static Map<String, String> getAPITags(Double lon, Double lat) {
        Map<String, String> tags = new HashMap<>();
        tags.put(LATITUDE_TAG, lat.toString());
        tags.put(LONGITUDE_TAG, lon.toString());
        tags.put(APP_KEY_TAG, API_KEY);
        return tags;
    }

    /**
     * @return an object to call the API with
     */
    public static APIInterface getAPICaller() {
        return APIClient.getClient().create(APIInterface.class);
    }
}

