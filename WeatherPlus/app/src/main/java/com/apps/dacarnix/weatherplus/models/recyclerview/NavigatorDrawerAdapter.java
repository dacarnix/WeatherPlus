package com.apps.dacarnix.weatherplus.models.recyclerview;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps.dacarnix.weatherplus.R;

import java.util.List;

/**
 * The adapter class for controlling how the recyclerview is populated
 * with information for the navigational drawer in the map activity.
 */
public class NavigatorDrawerAdapter extends RecyclerView.Adapter<NavigatorDrawerViewHolder> {

    private final NavigatorDrawerViewHolder.NavigatorItemClickListener mListener;
    private final List<NavigatorDrawerRowData> mData;

    /**
     * Constructor, takes in the listener to use in each row (to monitor mouse clicks)
     * and the list of data.
     *
     * @param listener to monitor presses
     * @param data     data to show to the user
     */
    public NavigatorDrawerAdapter(NavigatorDrawerViewHolder.NavigatorItemClickListener listener, List<NavigatorDrawerRowData> data) {
        mListener = listener;
        mData = data;
    }

    @NonNull
    @Override
    public NavigatorDrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_navigator_drawer_row, parent, false);
        return new NavigatorDrawerViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull NavigatorDrawerViewHolder holder, int position) {
        NavigatorDrawerRowData currentEntry = mData.get(position);
        holder.updateEntries(currentEntry);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

}
