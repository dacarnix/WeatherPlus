package com.apps.dacarnix.weatherplus.helpers;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import com.apps.dacarnix.weatherplus.R;

/**
 * A static class containing helper methods related
 * to building alert dialogs specifically for the location activity
 */
public class LocationActivityAlertBuilderHelper {

    /**
     * Returns a simple dialog displying help to the user
     *
     * @param context required to create the activity
     * @return A dialog
     */
    public static AlertDialog getHelpDialog(@NonNull Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.title_help_dialog);
        builder.setMessage(R.string.msg_help_dialog);
        builder.setPositiveButton(R.string.action_about_button, null);
        return builder.create();
    }
}

