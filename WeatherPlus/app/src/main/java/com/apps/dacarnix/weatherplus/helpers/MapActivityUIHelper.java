package com.apps.dacarnix.weatherplus.helpers;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.dacarnix.weatherplus.R;
import com.apps.dacarnix.weatherplus.models.WeatherData;

/**
 * A helper class containing methods to help animate views on the
 * MainActivity class and change UI components
 */
public class MapActivityUIHelper {
    private static final long SMOOTH_SLIDE_TME = 250;
    private static final long INSTANT_SLIDE_TME = 1;

    public static final int FAB_SAVED_LOCATION_IMAGE = R.drawable.ic_search_black_24dp;
    private static final int FAB_ADD_LOCATION_IMAGE = R.drawable.ic_add_location_black_24dp;

    private boolean mSettingsOpen = false;

    // boolean to track what the action of the 2nd fab is, either add location or edit
    private boolean mActionFABIsAddLocation = true;

    /**
     * It will prompt the change in states (ie if the user clicks on a new location,
     * which prompts a NEW marker, this will show the add location icon).
     * However, if the user were to click on an existing marker, it would go to
     * the edit location icon.
     * <p>
     * Makes use of different animation techniques than used for the setting fragment.
     *
     * @param action_location the floating action button view for the location action
     * @param savedLocation   whether the user clicked on a saved location
     * @param connected       whether there is internet connection
     */
    public void animateLocationFabButton(final FloatingActionButton action_location,
                                         boolean savedLocation, boolean connected) {
        mActionFABIsAddLocation = !savedLocation;
        if (!savedLocation) {
            action_location.setImageResource(FAB_ADD_LOCATION_IMAGE);
            if (!connected) {
                // We disable adding functionality while it is not connected to internet
                // Make sure it is hidden
                action_location.setVisibility(View.INVISIBLE);
                return;
            }
        } else {
            action_location.setImageResource(FAB_SAVED_LOCATION_IMAGE);
        }

        if (action_location.getVisibility() == View.INVISIBLE) {
            action_location.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Animates the settings preference fragment on the MainActivity
     * to either slide up or down (and become visible/gone) depending
     * on the current state.
     * <p>
     * It will also hide the search menu options when settings are active in the toolbar
     *
     * @param view             the setting fragment view container
     * @param searchMenuView   disables the search view in the tool bar while settings are active
     * @param hiddenY          the Y location to hide the view
     * @param visibleY         the Y location which makes the view visible
     * @param instant          whether the animation should be instant
     * @param networkConnected whether or not the app has network connection
     */
    public void animateSettingsFragmentMapActivity(final View view,
                                                   MenuItem searchMenuView, final float hiddenY,
                                                   final float visibleY,
                                                   boolean instant,
                                                   boolean networkConnected) {
        if (view.getVisibility() == View.GONE) {
            mSettingsOpen = true;
            slideDownInY(view, hiddenY, visibleY, (!instant) ? SMOOTH_SLIDE_TME : INSTANT_SLIDE_TME);
            searchMenuView.setVisible(false);
            searchMenuView.getActionView().setVisibility(View.GONE);
        } else {
            mSettingsOpen = false;
            slideUpInY(view, visibleY, hiddenY, (!instant) ? SMOOTH_SLIDE_TME : INSTANT_SLIDE_TME);
            // If the app has no network connection we do not want to enable the search bar
            if (networkConnected) {
                searchMenuView.setVisible(true);
                searchMenuView.getActionView().setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * Slides a given view down in the Y plane from the startY to the targetY
     * The animation will take the length defined in ANIMATION_SLIDE_TIME_MS.
     * This also sets the view to visible
     *
     * @param view          the view to slide up
     * @param startY        the start Y position of the view
     * @param targetY       the target Y position of the view
     * @param animationTime the time to take
     */
    private void slideDownInY(final View view,
                              final float startY,
                              final float targetY,
                              final long animationTime) {
        // Ensure it starts at the start Y
        view.setY(startY);
        view.setVisibility(View.VISIBLE);
        view.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        ObjectAnimator slideIn = ObjectAnimator.ofFloat(view,
                                View.TRANSLATION_Y,
                                startY,
                                targetY);
                        slideIn.setInterpolator(new DecelerateInterpolator());
                        slideIn.setDuration(animationTime);
                        slideIn.start();
                    }
                }
        );
    }

    /**
     * Slides a given view up in the Y plane from the startY to the targetY
     * The animation will take the length defined in ANIMATION_SLIDE_TIME_MS.
     * This also sets the view to invisible once the animation is complete
     *
     * @param view          the view to slide up
     * @param startY        the start Y position of the view
     * @param targetY       the target Y position of the view
     * @param animationTime the time to animate
     */
    private void slideUpInY(final View view,
                            float startY,
                            float targetY,
                            long animationTime) {
        ObjectAnimator slideOut = ObjectAnimator.ofFloat(view,
                View.TRANSLATION_Y,
                startY,
                targetY);
        slideOut.setInterpolator(new AccelerateInterpolator());
        slideOut.setDuration(animationTime);
        slideOut.start();
        slideOut.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                //Make it dissapear
                view.setVisibility(View.GONE);
                super.onAnimationEnd(animation);
            }
        });
    }

    /**
     * A method which helps update the navigational drawers header
     * with the appropriate UI based on the weather data (which may be null!)
     *
     * @param context the context to set the image resource icon
     * @param view    the navigator view
     * @param data    the weather data
     */
    public void updateNavigatorDrawerHeader(Context context, NavigationView view, @Nullable WeatherData data) {
        View header = view.getHeaderView(0);
        TextView textView = (TextView) header.findViewById(R.id.text_no_weather_data);
        View weatherInfoContainer = header.findViewById(R.id.weather_info_container);
        if (data != null) {
            textView.setVisibility(View.GONE);
            weatherInfoContainer.setVisibility(View.VISIBLE);

            TextView textTemp = (TextView) weatherInfoContainer.findViewById(R.id.text_temp);
            TextView textHumidity = (TextView) weatherInfoContainer.findViewById(R.id.text_hum);

            textTemp.setText(data.getTempInCelcius());
            textHumidity.setText(data.getHumidityAsString());

            ImageView icon = (ImageView) weatherInfoContainer.findViewById(R.id.image_weather_icon);
            icon.setBackgroundResource(data.getIconResourceId(context));

        } else {
            textView.setVisibility(View.VISIBLE);
            weatherInfoContainer.setVisibility(View.GONE);
        }
    }

    public boolean getSettingsOpen() {
        return mSettingsOpen;
    }

    /**
     * Whether or not the fab is for adding a new location
     *
     * @return ^
     */
    public boolean isAddFab() {
        return mActionFABIsAddLocation;
    }
}
