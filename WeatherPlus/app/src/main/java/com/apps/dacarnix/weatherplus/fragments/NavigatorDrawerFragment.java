package com.apps.dacarnix.weatherplus.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps.dacarnix.weatherplus.R;
import com.apps.dacarnix.weatherplus.models.recyclerview.NavigatorDrawerAdapter;
import com.apps.dacarnix.weatherplus.models.recyclerview.NavigatorDrawerRowData;
import com.apps.dacarnix.weatherplus.models.recyclerview.NavigatorDrawerViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * The fragment containing the drawer layout, and recyclerlist
 * showing further options.
 */
public class NavigatorDrawerFragment extends Fragment {
    @Nullable
    private NavigatorDrawerFragmentListener mListener;
    @Nullable
    private RecyclerView mNavigatorLists;
    @Nullable
    private Integer[] mImageTypeIDs;

    /**
     * Sets up the icons to use in the recyclerview
     */
    public NavigatorDrawerFragment() {
        mImageTypeIDs = new Integer[]{R.drawable.ic_location_city_black_24dp,
                R.drawable.ic_info_black_24dp};
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_navigator_drawer, container, false);
        mNavigatorLists = (RecyclerView) v.findViewById(R.id.recycler_view_navigator_drawer);
        setUpRecyclerView();
        return v;
    }

    /**
     * Get the context of the context the fragment is contained in
     * to send data to when clicked on and appropriate.
     *
     * @param context the context contained in
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof NavigatorDrawerFragmentListener) {
            mListener = (NavigatorDrawerFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement NavigatorDrawerFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * Load the data from the resources files to put on the the navigatordrawer.
     *
     * @return data to show
     */
    private List<NavigatorDrawerRowData> getData() {
        List<NavigatorDrawerRowData> data = new ArrayList<>();
        String[] titleData = getResources().getStringArray(R.array.navigation_menu_items);
        for (int i = 0; i < titleData.length; i++) {
            data.add(new NavigatorDrawerRowData(titleData[i], mImageTypeIDs[i]));
        }
        return data;
    }

    /**
     * Sets up the recyclerview that the drawer contains.
     * Ensures the correct click listeners are set up, and the right
     * icons to match each view shown.
     */
    private void setUpRecyclerView() {
        final List<NavigatorDrawerRowData> data = getData();
        NavigatorDrawerViewHolder.NavigatorItemClickListener listener = new NavigatorDrawerViewHolder.NavigatorItemClickListener() {
            @Override
            public void onClick(int position) {
                NavigatorDrawerRowData selectedEntry = data.get(position);
                if (mListener != null) {
                    mListener.onEntryClick(selectedEntry.getImageId());
                }
            }
        };

        NavigatorDrawerAdapter mListAdapter = new NavigatorDrawerAdapter(listener, data);
        mNavigatorLists.setAdapter(mListAdapter);

        RecyclerView.LayoutManager layout = new LinearLayoutManager(getActivity());
        mNavigatorLists.setLayoutManager(layout);

        // Add a line separator between each row
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(
                mNavigatorLists.getContext(),
                DividerItemDecoration.VERTICAL);
        mNavigatorLists.addItemDecoration(mDividerItemDecoration);
        mNavigatorLists.setHasFixedSize(true);
    }

    /**
     * A simple interface to enforce the ability to pass data from this fragment
     * to any activity which possesses it.
     */
    public interface NavigatorDrawerFragmentListener {
        void onEntryClick(int uniqueResource);
    }

}
