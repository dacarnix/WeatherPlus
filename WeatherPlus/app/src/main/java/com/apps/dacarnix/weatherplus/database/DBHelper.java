package com.apps.dacarnix.weatherplus.database;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.apps.dacarnix.weatherplus.app.App;
import com.apps.dacarnix.weatherplus.database.model.LocationContract;
import com.apps.dacarnix.weatherplus.database.model.WeatherContract;
import com.apps.dacarnix.weatherplus.database.repo.LocationRepo;
import com.apps.dacarnix.weatherplus.database.repo.WeatherRepo;

/**
 * DatabaseHelper, holds useful information, constants and methods
 * to using and accessing the database on the device.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "weatherDB.db";

    public DBHelper() {
        super(App.getContext(), DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(LocationRepo.createTable());
        db.execSQL(WeatherRepo.createTable());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + LocationContract.LocationEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + WeatherContract.WeatherEntry.TABLE_NAME);
        onCreate(db);
    }

    public static String getColumnData(Cursor cursor, String column) {
        return cursor.getString(cursor.getColumnIndex(column));
    }

    public static double getColumnDouble(Cursor cursor, String column) {
        return Double.parseDouble(getColumnData(cursor, column));
    }

    public static Long getColumnLong(Cursor cursor, String column) {
        return Long.parseLong(getColumnData(cursor, column));
    }

    public static Integer getColumnInt(Cursor cursor, String column) {
        return Integer.parseInt(getColumnData(cursor, column));
    }
}
