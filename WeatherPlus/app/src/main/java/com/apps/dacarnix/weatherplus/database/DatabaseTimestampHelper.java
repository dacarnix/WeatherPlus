package com.apps.dacarnix.weatherplus.database;


/**
 * Contains methods related to timestamp data for storage in the database
 */
public class DatabaseTimestampHelper {

    /**
     * Returns the unix time stamp
     *
     * @return the unix time
     */
    public static long getCurrentUnixTime() {
        return System.currentTimeMillis() / 1000L;
    }
}
