package com.apps.dacarnix.weatherplus.app;

import android.app.Application;
import android.content.Context;

import com.apps.dacarnix.weatherplus.database.DBHelper;
import com.apps.dacarnix.weatherplus.database.DBManager;

/**
 * An extension to the application.
 * This is used in maintaining the database by
 * providing a consistent context.
 */
public class App extends Application {
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this.getApplicationContext();
        DBHelper dbHelper = new DBHelper();
        DBManager.initializeInstance(dbHelper);
    }

    public static Context getContext() {
        return context;
    }

}