package com.apps.dacarnix.weatherplus.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.ViewModel;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v7.preference.PreferenceManager;

import com.apps.dacarnix.weatherplus.R;
import com.apps.dacarnix.weatherplus.database.DatabaseTimestampHelper;
import com.apps.dacarnix.weatherplus.database.repo.LocationRepo;
import com.apps.dacarnix.weatherplus.database.repo.WeatherRepo;
import com.apps.dacarnix.weatherplus.helpers.MapActivityConstants;
import com.apps.dacarnix.weatherplus.models.Location;
import com.apps.dacarnix.weatherplus.models.WeatherData;
import com.apps.dacarnix.weatherplus.models.weatherapi.APIInterface;
import com.apps.dacarnix.weatherplus.models.weatherapi.APIUtils;
import com.apps.dacarnix.weatherplus.models.weatherapi.data.WeatherResponse;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A means to get storage of the saved locations in a list, based on the current preference
 * filters used in the MainActivity / MyMapFragment. This should persist over state changes
 * and as it pulls from the database, it will be of great help to not require constant re-pulling of
 * data. It will also mitigate memory leaks / issues on state changes while a database pull is occuring.
 * <p>
 * This will also serve as a means to cleanly conduct the API calls such that on rotation
 * there will be no issues.
 * <p>
 * API information will be stored in a database, and if applicable, retrieved from there instead
 * of re-called.
 */
public class WeatherAndLocationsViewModel extends ViewModel {

    @Nullable
    private LatLng mLastPosition = null;

    // Caches the last known position and weather data for current location
    @Nullable
    private WeatherData mWeatherData = null;
    // To not overload the API calls, and the fact weather doesnt change often, ensure we are not
    // making many requests to a similar location too fast.
    private Long mTimeOfCurrentWeatherRequest;

    private List<Location> mSavedLocations = new ArrayList<>();
    private APIInterface mApiCaller = APIUtils.getAPICaller();

    // The filter to use on the markers, this is set via the preferencefragment
    @Nullable
    private String mPreferenceFilter = null;

    // To keep track if a deletion in the saved locations occured for UI purposes
    private boolean mDeleteOccured = false;

    // Maps a unique location ID to weather information
    private Map<Integer, WeatherData> mLocationIdToWeather = new HashMap<>();

    /**
     * Based on the saved locations loaded from the database, and the current filter
     * return a list of usage markers for the google maps.
     * <p>
     * This viewmodel does not keep track of markers as they are a part of the view.
     *
     * @param map the map to create markers for
     * @return a list of markers
     */
    public List<Marker> getMarkers(GoogleMap map) {
        List<Marker> toReturn = new ArrayList<>();
        for (int i = 0; i < mSavedLocations.size(); i++) {
            Location sl = mSavedLocations.get(i);
            Marker toAdd = map.addMarker(
                    new MarkerOptions()
                            .title(sl.getName())
                            .position(new LatLng(sl.getLatitude(), sl.getLongitude()))
            );
            toAdd.setTag(i);
            toReturn.add(toAdd);
        }
        return toReturn;
    }

    /**
     * Checks changes to the preferences and makes appropriate actions
     *
     * @param activity the activity which contains the settings fragment
     */
    public void checkChanges(Activity activity) {
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(activity.getBaseContext());
        if (SP.getBoolean(MapActivityConstants.SHOW_LOCATIONS_AS_MARKERS, false)) {
            int filterIndex = Integer.parseInt(SP.getString(MapActivityConstants.FITLER_LOCATION_MARKERS,
                    "0"));
            String filter = activity.getResources().getStringArray(R.array.location_types_filter)[filterIndex];

            if (mPreferenceFilter == null || !filter.equals(mPreferenceFilter)) {
                if (filter.equals(MapActivityConstants.FILTER_ALL_WILDCARD)) filter = "";
                mPreferenceFilter = filter;
                refreshQuery();
            }
        }
    }

    /**
     * This is called whenever there was a direct change to the database.
     * Either a deletion or an insertion of a saved location
     * <p>
     * For any location with missing weather data, or out of date, attempt to get new data.
     */
    public void refreshQuery() {
        int previousCount = mSavedLocations.size();
        mSavedLocations = LocationRepo.getRowsAsLocation(mPreferenceFilter);
        for (final Location loc : mSavedLocations) {
            checkLocationWeatherData(loc);
        }
        int newSize = mSavedLocations.size();
        mDeleteOccured = previousCount > newSize;
    }

    /**
     * For a given location, sees if there is valid weather data in the database.
     * And if not, update it accordingly.
     * <p>
     * It will then cache these and therefore removes the need for excessive database calls
     *
     * @param loc the location
     */
    private void checkLocationWeatherData(final Location loc) {
        final WeatherRepo repo = new WeatherRepo();
        final Integer uniqueLocationKey = loc.getId();
        WeatherData data = null;
        // Check cache first
        if (mLocationIdToWeather.containsKey(uniqueLocationKey)) {
            data = mLocationIdToWeather.get(uniqueLocationKey);
        }
        // If not in cache, check database
        if (data != null) {
            data = WeatherRepo.getWeatherForLocation(loc.getId());
        }

        long currentUnixTime = DatabaseTimestampHelper.getCurrentUnixTime();
        // If data doesnt exist, or its over an hour old, request it.
        if (data == null || currentUnixTime - data.getUnixTimeOfData()
                > MapActivityConstants.WEATHER_DATA_TIMEOUT) { // One hour
            getWeather(new LatLng(loc.getLatitude(), loc.getLongitude()), new WeatherDataDelegate() {
                @Override
                public void onSuccess(WeatherResponse response) {
                    // Get the weather data and store it in the database for future use
                    WeatherData data = response.toWeatherData();
                    data.setLocationId(uniqueLocationKey);
                    repo.insert(data);
                    mLocationIdToWeather.put(uniqueLocationKey, data);
                }
            });
        }
    }

    public boolean deletionOccured() {
        return mDeleteOccured;
    }

    /**
     * Checks for the weather at a given position, and then runs the respective delegate.
     *
     * @param position the position to check weather at
     * @param delegate how to act on API response
     */
    public void getWeather(LatLng position, final WeatherDataDelegate delegate) {
        // Check database...
        mApiCaller.getWeatherAtLocation(APIUtils.getAPITags(position.longitude, position.latitude))
                .enqueue(new Callback<WeatherResponse>() {
                    @Override
                    public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> weatherResponse) {

                        if (weatherResponse.isSuccessful()) {
                            delegate.onSuccess(weatherResponse.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<WeatherResponse> call, Throwable t) {
                        // Data base will have null values and therefore no need to change anything
                    }
                });
    }

    /**
     * Sets the current position.
     * If it has been over 60 seconds since the last time the weather for the current position
     * has been updated, OR it has never been updated, a call will me made to get the current weather.
     *
     * @param position the position of the user
     */
    public void setCurrentPosition(LatLng position) {
        mLastPosition = position;
        long currentTime = DatabaseTimestampHelper.getCurrentUnixTime();
        if (mTimeOfCurrentWeatherRequest == null || currentTime - mTimeOfCurrentWeatherRequest > 60) {
            mTimeOfCurrentWeatherRequest = currentTime;
            getWeather(position, new WeatherDataDelegate() {
                @Override
                public void onSuccess(WeatherResponse response) {
                    mWeatherData = response.toWeatherData();
                }
            });
        }
    }

    /**
     * Returns the last position found
     *
     * @return the last current position
     */
    @Nullable
    public LatLng getCurrentPosition() {
        return mLastPosition;
    }

    /**
     * Returns the last known weather at the users location
     *
     * @return the users local weather
     */
    @Nullable
    public WeatherData getCurrentWeather() {
        return mWeatherData;
    }


    public interface WeatherDataDelegate {
        void onSuccess(WeatherResponse response);
    }
}
