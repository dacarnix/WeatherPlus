package com.apps.dacarnix.weatherplus.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.apps.dacarnix.weatherplus.R;
import com.apps.dacarnix.weatherplus.helpers.MapActivityConstants;
import com.apps.dacarnix.weatherplus.helpers.MapActivityMapsHelper;
import com.apps.dacarnix.weatherplus.viewmodels.WeatherAndLocationsViewModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.List;

/**
 * A fragment which holds all the extension methods, and variables
 * surrounded around the standard google maps API fragment.
 * This will hold much of the keeping track of markers placed and the floating action buttons related.
 */
public class MyMapFragment extends Fragment implements OnMapReadyCallback, LocationListener,
        GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener {

    // Map related variables
    private GoogleMap mGoogleMap;
    private LocationManager mLocationManager;
    private String mBestProvider;
    private boolean mPendingMyLocationRequest = false;
    private int mPendingZoomRequest;
    // Map marker related variables
    @Nullable
    private Marker mOnTapMarker;
    private int mSelectedMarkerIndex = MapActivityConstants.NO_MARKER_SELECTED;
    private boolean mMarkerActive = false;
    @Nullable
    private List<Marker> mSavedMarkers;
    // The data contained is used in the google maps, and therefore must use it when the maps is ready async.
    private Bundle mBundle;

    private WeatherAndLocationsViewModel mSavedViewModel;
    private LocationPermissionsListener mListener;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_map, container, false);
    }

    /**
     * Saves the data contained in the fragment so that
     * it remains persistent on state changes (such as a rotation).
     *
     * @param view               of the fragment
     * @param savedInstanceState where to save the data
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSavedViewModel = ViewModelProviders.of(getActivity()).get(WeatherAndLocationsViewModel.class);
        mBundle = savedInstanceState;
        setUpMap();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof LocationPermissionsListener) {
            mListener = (LocationPermissionsListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement AddLocationFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onStop() {
        super.onStop();
        mLocationManager.removeUpdates(this);
    }

    /**
     * Called whenever the location provider gets our location.
     * If we were pending to go to "our location", then it is triggered.
     * Updates the location value in our viewmodel such that on orientation change our previously
     * calculated values are not wasted.
     *
     * @param location our location
     */
    @Override
    public void onLocationChanged(android.location.Location location) {
        LatLng currentPos = new LatLng(location.getLatitude(), location.getLongitude());
        mSavedViewModel.setCurrentPosition(currentPos);
        if (mPendingMyLocationRequest) {
            goToLocation(currentPos, mPendingZoomRequest, true);
            mPendingMyLocationRequest = false;
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    /**
     * Only consume marker clicks IF theyre on a marker which is already saved (ie a red one)
     *
     * @param marker the marker clicked on
     * @return we want the google maps default behaviour, therefore we return false.
     */
    @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker.getTitle().equals(MapActivityMapsHelper.CLICKED_LOCATION_STRING)) return true;
        savedMarkerClicked(marker);
        return false;
    }

    @Override
    public void onMapClick(LatLng point) {
        addNonSavedMarker(point);
    }

    /**
     * We want to save the marker that the user has clicked on and retain it on rotation
     *
     * @param state where to save it.
     */
    @Override
    public void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        if (mOnTapMarker != null) {
            state.putBoolean(MapActivityConstants.BUNDLE_SELECTED_MARKER_SELECTED, mMarkerActive);
            state.putDouble(MapActivityConstants.BUNDLE_SELECTED_MARKER_LAT, mOnTapMarker.getPosition().latitude);
            state.putDouble(MapActivityConstants.BUNDLE_SELECTED_MARKER_LONG, mOnTapMarker.getPosition().longitude);
            state.putInt(MapActivityConstants.BUNDLE_SELECTED_MARKER_INDEX,
                    Integer.parseInt(mOnTapMarker.getTag().toString()));
        }
    }

    /**
     * Called when the map is ready to be used. This will check the bundle for a previously saved
     * marker click, and if so, simulate it such that no "view" is lost on rotation.
     *
     * @param googleMap the map to now use and set up listeners for.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setOnMapClickListener(this);
        mGoogleMap.setOnMarkerClickListener(this);
        mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
        refreshSavedMarkers();
        if (mBundle != null) { // Restore information regarding markers on rotation
            mSelectedMarkerIndex = mBundle.getInt(MapActivityConstants.BUNDLE_SELECTED_MARKER_INDEX);
            if (mSelectedMarkerIndex != MapActivityConstants.NO_MARKER_SELECTED) {

                mMarkerActive = mBundle.getBoolean(MapActivityConstants.BUNDLE_SELECTED_MARKER_SELECTED);
                double lat = mBundle.getDouble(MapActivityConstants.BUNDLE_SELECTED_MARKER_LAT);
                double lon = mBundle.getDouble(MapActivityConstants.BUNDLE_SELECTED_MARKER_LONG);

                if (mSelectedMarkerIndex >= 0) { // It is a saved marker clicked on
                    mOnTapMarker = mSavedMarkers.get(mSelectedMarkerIndex);
                    if (mMarkerActive) {
                        simulateMarkerClick(mOnTapMarker);
                    }
                } else {
                    // Not a saved marker
                    mOnTapMarker = mGoogleMap.addMarker(
                            MapActivityMapsHelper.createNewMarker(new LatLng(lat, lon)));
                    mOnTapMarker.setTag(mSelectedMarkerIndex);
                    mListener.onMarkerClick(false);
                }
            }
        }
        // This was the very first time opening the application, go to your current position
        if (mBundle == null) {
            myLocationRequest();
        }
    }

    /**
     * Pulls the saved locations from the viewmodel and updates the markers accordingly
     */
    public void refreshSavedMarkers() {
        mSavedViewModel.refreshQuery();
        if (mSavedMarkers != null) {
            for (Marker m : mSavedMarkers) m.remove();
        }
        if (mGoogleMap != null) {
            mSavedMarkers = mSavedViewModel.getMarkers(mGoogleMap);
        }
    }

    /**
     * Sets up the google maps API and location services
     */
    private void setUpMap() {
        SupportMapFragment mapFragment = (SupportMapFragment)
                getChildFragmentManager().findFragmentById(R.id.map_google);
        mapFragment.getMapAsync(this);
        mLocationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
    }

    /**
     * Attempts to request current location information
     */
    private void getLocationInformation() {
        if (mListener.checkLocationPermission()) {
            if (mBestProvider == null) {
                mBestProvider = mLocationManager.getBestProvider(MapActivityMapsHelper.getCriteria(), true);
            }

            android.location.Location location = mLocationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            if (location != null)
                onLocationChanged(location);

            mLocationManager.requestLocationUpdates(mBestProvider, MapActivityConstants.MINIMUM_LOCATION_REFRESH_TIME,
                    MapActivityConstants.MINIMUM_LOCATION_DISTANCE, this);
        }
    }

    /**
     * Attempts to go to a location if it is not null.
     * If it is null, and it is due to not receiving current location yet,
     * it will create a pending request.
     *
     * @param location   location to go to
     * @param zoom       the zoom level to use
     * @param myLocation whether or not the location supplied is the current location of the user
     */
    public void goToLocation(LatLng location, int zoom, boolean myLocation) {
        if (myLocation) location = mSavedViewModel.getCurrentPosition();
        if (mListener.checkLocationPermission() && mGoogleMap != null) {
            if (location != null) {
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, zoom));
                addNonSavedMarker(location);
            } else if (myLocation) {
                Toast.makeText(getContext(), R.string.msg_toast_my_location, Toast.LENGTH_SHORT).show();
                mPendingMyLocationRequest = true;
                mPendingZoomRequest = zoom;
                getLocationInformation();
            }
        }
    }

    public boolean isLocationServicesEnabled() {
        return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    /**
     * On rotation, we can't forge a click event therefore we try to simulate it
     *
     * @param marker the marker simulated
     */
    private void simulateMarkerClick(Marker marker) {
        marker.showInfoWindow();
        savedMarkerClicked(marker);
    }

    private void savedMarkerClicked(Marker marker) {
        clearPreviousMarker();
        mSelectedMarkerIndex = Integer.parseInt(marker.getTag().toString());
        mOnTapMarker = marker;
        mMarkerActive = true;
        mListener.onMarkerClick(true);
    }

    /**
     * Clears the previous tapped marker if it was a non-saved marker from the map
     */
    private void clearPreviousMarker() {
        if (mOnTapMarker != null && mOnTapMarker.getTitle().equals(MapActivityMapsHelper.CLICKED_LOCATION_STRING)) {
            mOnTapMarker.remove();
        }
        mMarkerActive = false;
    }

    /**
     * Updates the non-saved click marker on the map, and removes any previous trace of it
     *
     * @param point the point to create it at
     */
    private void addNonSavedMarker(LatLng point) {
        clearPreviousMarker();
        mOnTapMarker = mGoogleMap.addMarker(MapActivityMapsHelper.createNewMarker(point));
        mOnTapMarker.setTag(MapActivityConstants.ON_TAP_MARKER_INDEX);
        mSelectedMarkerIndex = MapActivityConstants.ON_TAP_MARKER_INDEX;
        mListener.onMarkerClick(false);
    }

    public void myLocationRequest() {
        mPendingMyLocationRequest = true;
        mPendingZoomRequest = MapActivityConstants.BROAD_ZOOM_LEVEL;
        getLocationInformation();
    }

    public Marker getOnTapMarker() {
        return mOnTapMarker;
    }

    public boolean selectedSavedMarker() {
        return mSelectedMarkerIndex >= 0;
    }

    /**
     * When the user saves a marker, we want to transfer the blue "temp" marker, to become
     * a red one and simulate the on click.
     */
    public void savedCurrentMarker() {
        refreshSavedMarkers();
        if (mSavedMarkers != null && mOnTapMarker != null && mSavedMarkers.size() > 0) {
            // Set the active marker to be the last one in the list
            mSelectedMarkerIndex = mSavedMarkers.size() - 1;
            mOnTapMarker.remove();
            mOnTapMarker = mSavedMarkers.get(mSelectedMarkerIndex);
            savedMarkerClicked(mOnTapMarker);
            mOnTapMarker.showInfoWindow();
        }
    }

    /**
     * Clears any selected markers
     */
    public void clearSelectedMarkers() {
        clearPreviousMarker();
        mOnTapMarker = null;
        refreshSavedMarkers();
    }

    /**
     * The interface used to help communicate with checking for permissions back in the activity
     * which uses this fragment.
     */
    public interface LocationPermissionsListener {
        boolean checkLocationPermission();

        void onMarkerClick(boolean savedLocation);
    }
}
