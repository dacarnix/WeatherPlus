package com.apps.dacarnix.weatherplus.models.weatherapi.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Holds data regarding the temperature, used as the java object to convert in to from the JSON
 * response given via openweathermap.
 */
public class Temperature {

    // The API returns the temperature in KELVIN
    @SerializedName("temp")
    @Expose
    private Double mTemperature;

    @SerializedName("humidity")
    @Expose
    private Double mHumidity;

    public Double getTemp() {
        return mTemperature;
    }

    public Double getHumidity() {
        return mHumidity;
    }
}