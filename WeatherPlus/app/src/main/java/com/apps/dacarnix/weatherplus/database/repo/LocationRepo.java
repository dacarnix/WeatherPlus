package com.apps.dacarnix.weatherplus.database.repo;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.apps.dacarnix.weatherplus.database.DBHelper;
import com.apps.dacarnix.weatherplus.database.DBManager;
import com.apps.dacarnix.weatherplus.database.model.LocationContract;
import com.apps.dacarnix.weatherplus.models.Location;

import java.util.ArrayList;
import java.util.List;

/**
 * A class for handling the database operations surronding the saved location tables.
 * Contains methods for retrieving, deleting and creating rows.
 */
public class LocationRepo {

    /**
     * @return the SQL query for creating the table with the correct
     * column names and types for the saved location
     */
    public static String createTable() {
        return "CREATE TABLE " + LocationContract.LocationEntry.TABLE_NAME + "("
                + LocationContract.LocationEntry._ID + "   INTEGER PRIMARY KEY AUTOINCREMENT   ,"
                + LocationContract.LocationEntry.COLUMN_NAME_NAME + " TEXT ,"
                + LocationContract.LocationEntry.COLUMN_NAME_COUNTRY + " TEXT ,"
                + LocationContract.LocationEntry.COLUMN_NAME_LAT + " REAL ,"
                + LocationContract.LocationEntry.COLUMN_NAME_LONG + " REAL ,"
                + LocationContract.LocationEntry.COLUMN_NAME_TIMESTAMP + " INTEGER  ,"
                + LocationContract.LocationEntry.COLUMN_NAME_TYPE + " TEXT )";
    }

    /**
     * @return a cursor object pointing to all rows contained, along with their unique row_id
     */
    public static Cursor getAllRows() {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();
        return db.rawQuery("SELECT * FROM " + LocationContract.LocationEntry.TABLE_NAME, null);
    }

    /**
     * Returns the rows as location objects which have a type that matches the type filter
     *
     * @param typeFilter the filter
     * @return all objects which have a type equal
     */
    public static List<Location> getRowsAsLocation(String typeFilter) {
        List<Location> locations = new ArrayList<>();
        try (Cursor cursor = getRowsThatTypeEqual(typeFilter)) {
            while (cursor.moveToNext()) {
                locations.add(fromCursor(cursor));
            }
        }
        return locations;
    }

    /**
     * The rows will be filtered by ones which contain the string CONTAINS in either
     * their country, name, or type
     *
     * @param contains the string to check for
     * @return a cursor object pointing to filtered rows, along with their unique ID
     */
    public static Cursor getRowsThatContain(String contains) {
        contains = '%' + contains + '%'; // surrond with wildcards
        SQLiteDatabase db = DBManager.getInstance().openDatabase();
        return db.rawQuery("SELECT * FROM " + LocationContract.LocationEntry.TABLE_NAME + " WHERE "
                        + LocationContract.LocationEntry.COLUMN_NAME_NAME + " LIKE ? OR "
                        + LocationContract.LocationEntry.COLUMN_NAME_COUNTRY + " LIKE ? OR "
                        + LocationContract.LocationEntry.COLUMN_NAME_TYPE + " LIKE ?",
                new String[]{contains, contains, contains});
    }


    /**
     * The rows will be filtered by ones which ones have a type equal to CONTAINS
     *
     * @param contains the string to check for
     * @return a cursor object pointing to filtered rows
     */
    private static Cursor getRowsThatTypeEqual(String contains) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();
        return db.rawQuery("SELECT * FROM " + LocationContract.LocationEntry.TABLE_NAME + " WHERE "
                        + LocationContract.LocationEntry.COLUMN_NAME_TYPE + " LIKE ?",
                new String[]{'%' + contains + '%'});
    }

    /**
     * Inserts a row based on the location provided
     *
     * @param loc the location to inset
     * @return the ID that the row got
     */
    public int insert(Location loc) {
        int locId;

        SQLiteDatabase db = DBManager.getInstance().openDatabase();
        ContentValues values = new ContentValues();

        values.put(LocationContract.LocationEntry.COLUMN_NAME_NAME, loc.getName());
        values.put(LocationContract.LocationEntry.COLUMN_NAME_COUNTRY, loc.getCountry());
        values.put(LocationContract.LocationEntry.COLUMN_NAME_LAT, loc.getLatitude());
        values.put(LocationContract.LocationEntry.COLUMN_NAME_LONG, loc.getLongitude());
        values.put(LocationContract.LocationEntry.COLUMN_NAME_TIMESTAMP, loc.getTimeStamp());
        values.put(LocationContract.LocationEntry.COLUMN_NAME_TYPE, loc.getType());

        // Inserting Row
        locId = (int) db.insert(LocationContract.LocationEntry.TABLE_NAME, null, values);
        DBManager.getInstance().closeDatabase();

        return locId;
    }

    /**
     * Deletes a row based on their unique ID
     *
     * @param id the unique id
     */
    public void deleteRecord(Integer id) {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();
        db.delete(LocationContract.LocationEntry.TABLE_NAME,
                LocationContract.LocationEntry._ID + "=?",
                new String[]{id.toString()});
        DBManager.getInstance().closeDatabase();

        // Also delete any weather data associated
        WeatherRepo repo = new WeatherRepo();
        repo.deleteRecord(id);
    }

    /**
     * Deletes the entire table
     */
    public void deleteAll() {
        SQLiteDatabase db = DBManager.getInstance().openDatabase();
        db.delete(LocationContract.LocationEntry.TABLE_NAME, null, null);
        DBManager.getInstance().closeDatabase();
    }

    /**
     * Loads a location object from the current row the cursor is pointing at
     * by extracting the column data.
     *
     * @param cursor the cursor object
     * @return the location based on the current cursor
     */
    public static Location fromCursor(Cursor cursor) {
        Location location = new Location(DBHelper.getColumnData(cursor, LocationContract.LocationEntry.COLUMN_NAME_TYPE),
                DBHelper.getColumnData(cursor, LocationContract.LocationEntry.COLUMN_NAME_NAME),
                DBHelper.getColumnDouble(cursor, LocationContract.LocationEntry.COLUMN_NAME_LONG),
                DBHelper.getColumnDouble(cursor, LocationContract.LocationEntry.COLUMN_NAME_LAT),
                DBHelper.getColumnData(cursor, LocationContract.LocationEntry.COLUMN_NAME_COUNTRY),
                DBHelper.getColumnLong(cursor, LocationContract.LocationEntry.COLUMN_NAME_TIMESTAMP));
        location.setId(DBHelper.getColumnInt(cursor, LocationContract.LocationEntry._ID));
        return location;
    }
}
