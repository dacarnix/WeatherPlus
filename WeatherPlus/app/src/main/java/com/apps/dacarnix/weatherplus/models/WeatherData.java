package com.apps.dacarnix.weatherplus.models;

import android.content.Context;
import android.support.annotation.Nullable;

import com.apps.dacarnix.weatherplus.models.weatherapi.APIUtils;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * A basic class which will hold information about weather at a given
 * location, at a given time.
 */
public class WeatherData {

    // Used to map the icon tag in the JSON return to the icon drawable in our resources
    private static final String ICON_TAG_TO_DRAWABLE = "ic_weather_";

    @Nullable
    private Double mTemperature;
    @Nullable
    private Double mHumidity;
    @Nullable
    private String mWeatherDescription;
    @Nullable
    private String mIconTag;
    // Stored in unix time
    @Nullable
    private Long mTimeOfEntry;

    // The ID of the row of the saved location in the database associated with this weather report
    @Nullable
    private Integer mLocId;

    public WeatherData(@Nullable Double temp,
                       @Nullable Double humidity,
                       @Nullable String weatherDesc,
                       @Nullable String iconTag,
                       @Nullable Long timeOfData) {
        mTemperature = temp;
        mHumidity = humidity;
        mWeatherDescription = weatherDesc;
        mIconTag = iconTag;
        mTimeOfEntry = timeOfData;
    }

    public void setId(Integer id) {
        Integer mId = id;
    }

    public void setLocationId(Integer id) {
        mLocId = id;
    }

    public Integer getLocationId() {
        return mLocId;
    }

    public Double getTemperature() {
        return mTemperature;
    }

    public Double getHumidity() {
        return mHumidity;
    }

    public String getDescription() {
        return mWeatherDescription;
    }

    public String getIconTag() {
        return mIconTag;
    }

    public Long getUnixTimeOfData() {
        return mTimeOfEntry;
    }

    /**
     * Returns the ID for the drawable file associated with the icon tag
     *
     * @param context the context required to access the files
     * @return the ID
     */
    public int getIconResourceId(Context context) {
        return context.getResources().getIdentifier(ICON_TAG_TO_DRAWABLE + mIconTag,
                "drawable", context.getPackageName());
    }

    /**
     * Converts the temperature in to celcius, from kelvin and formats it.
     *
     * @return the celcius temperature
     */
    public String getTempInCelcius() {
        Double newTemp = getTemperature() + APIUtils.KELVIN_TO_CELCIUS_OFFSET;
        NumberFormat formatter = new DecimalFormat("#0");
        return formatter.format(newTemp) + "C";
    }

    public String getHumidityAsString() {
        NumberFormat formatter = new DecimalFormat("#0");
        return formatter.format(getHumidity()) + "%";
    }

}
