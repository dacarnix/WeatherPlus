package com.apps.dacarnix.weatherplus.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.apps.dacarnix.weatherplus.R;
import com.apps.dacarnix.weatherplus.database.repo.LocationRepo;
import com.apps.dacarnix.weatherplus.fragments.AddLocationFragment;
import com.apps.dacarnix.weatherplus.helpers.MapActivityConstants;
import com.apps.dacarnix.weatherplus.models.GetCountryAsyncTaskLoader;
import com.apps.dacarnix.weatherplus.models.Location;
import com.google.android.gms.maps.model.LatLng;

/**
 * An activity responsible for creating and saving a new location
 * to the database.
 * <p>
 * This also may have an entry point from an external location sharing
 * a given location with it.
 */
public class AddLocationActivity extends AppCompatActivity implements
        AddLocationFragment.AddLocationFragmentListener,
        LoaderManager.LoaderCallbacks<String> {

    // Variable used in the passing of bundles
    public static final String EXTRA_LONG = "com.apps.dacarnix.weatherplus.EXTRA_LONG";
    public static final String EXTRA_LAT = "com.apps.dacarnix.weatherplus.EXTRA_LAT";

    // The default string used while the app attempts to determine the country of the lat/long
    public static final String FINDING_COUNTRY_STRING = "Finding country..";

    // Used for the Loader class for async tasks involved in getting the country
    private static final int UNIQUE_ACTIVITY_CODE = 1111;

    /**
     * Attempts to launch this activity based on another.
     * This launch is based on a google maps marker information, and therefore
     * will take in a latlng position to save.
     *
     * @param activity the activity to launch from
     * @param position the position of the marker to add
     */
    public static void launchActivity(Activity activity, LatLng position) {
        Intent intent = new Intent(activity, AddLocationActivity.class);

        intent.putExtra(AddLocationActivity.EXTRA_LAT, position.latitude);
        intent.putExtra(AddLocationActivity.EXTRA_LONG, position.longitude);

        activity.startActivityForResult(intent, MapActivityConstants.UNIQUE_CODE_CREATE_LOCATION);
    }

    @Nullable
    private Double mLong;
    @Nullable
    private Double mLat;

    @Nullable
    private AddLocationFragment mFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_location);
        setToolbar();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mLong = bundle.getDouble(EXTRA_LONG);
            mLat = bundle.getDouble(EXTRA_LAT);

            mFragment = (AddLocationFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.fragment_add_location);

            String mCountry = FINDING_COUNTRY_STRING;
            mFragment.updateContent(mCountry, mLong, mLat);

            // Make an async request to find the country from the latlong
            getSupportLoaderManager().initLoader(0, null, this).forceLoad();
        }
    }

    /**
     * Allows the user to click on the back button to return to the main menu
     *
     * @param menuItem the item pressed
     * @return a boolean regarding if the event has been handled.
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    /**
     * A request to add the location has been created
     * from the internal fragment. Add it to the database
     * and then return to the main menu.
     * <p>
     * Present a toast signalling the success
     *
     * @param toAdd the location to add
     */
    @Override
    public void onLocationClick(Location toAdd) {
        try {
            LocationRepo locRepo = new LocationRepo();
            locRepo.insert(toAdd);
            Toast.makeText(this, R.string.msg_toast_save_success, Toast.LENGTH_SHORT).show();
            setResult(Activity.RESULT_OK);
        } catch (Exception ex) {
            setResult(Activity.RESULT_CANCELED);
        }
        finish();
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        return new GetCountryAsyncTaskLoader(this, new LatLng(mLat, mLong));
    }

    /**
     * When the async task (the country) is found, this will be called bringing
     * it back in to the UI thread. We then must update the fragment with the value.
     *
     * @param loader the loader
     * @param data   the data it responded with.
     */
    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        if (mFragment != null) {
            if (data.trim().isEmpty()) data = MapActivityConstants.DEFAULT_COUNTRY;
            mFragment.updateContent(data, mLong, mLat);
        }
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {

    }

    /**
     * Sets up the tool bar
     */
    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar action = getSupportActionBar();
        if (action != null) {
            action.setTitle(R.string.title_add_location_activity);
            action.setDisplayHomeAsUpEnabled(true);
            action.setHomeButtonEnabled(true);
        }
    }
}
