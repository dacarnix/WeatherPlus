package com.apps.dacarnix.weatherplus.models.weatherapi.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Used as a java object to parse JSON from openweathermap, using GSON.
 * Currently will only hold descriptions regarding the weather, and a string to identify
 * the appropriate icon to show to the user.
 */
public class Weather {

    @SerializedName("description")
    @Expose
    private String mDescription;

    @SerializedName("icon")
    @Expose
    private String mIconId;

    public String getDescription() {
        return mDescription;
    }

    public String getIcon() {
        return mIconId;
    }

}