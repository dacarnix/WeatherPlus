package com.apps.dacarnix.weatherplus.models.weatherapi;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A class which follows the singleton pattern to return a retrofit client.
 * The use of retrofit is the core of making API calls within this
 * project to ensure type-safety.
 */
public class APIClient {

    private static Retrofit retrofit = null;

    private APIClient() {

    }

    /**
     * Get the client, based on the singleton pattern.
     * Uses the BASE URL in the constant, to point at the weather API,
     * and ensures the use of a GSON converter.
     *
     * @return the retrofit object to make API calls with.
     */
    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(APIUtils.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}