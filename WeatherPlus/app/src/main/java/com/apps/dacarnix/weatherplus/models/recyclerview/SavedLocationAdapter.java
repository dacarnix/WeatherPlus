package com.apps.dacarnix.weatherplus.models.recyclerview;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps.dacarnix.weatherplus.R;
import com.apps.dacarnix.weatherplus.database.repo.LocationRepo;
import com.apps.dacarnix.weatherplus.helpers.LocationTypeHelper;
import com.apps.dacarnix.weatherplus.models.Location;

/**
 * The adapter class for controlling how the recyclerview is populated
 * with information for the Saved Locations list in the locations activity.
 * <p>
 * It also handles the mapping of a Location type to get its appropriate related icon.
 * <p>
 * This class inherits off of the CursorRecyclerViewAdapter, which allows for the usage
 * of a cursor being the underlying source of data, rather than being passed a list.
 */
public class SavedLocationAdapter extends CursorRecyclerViewAdapter<SavedLocationViewHolder> {

    private final SavedLocationViewHolder.SavedLocationListClickListener mListener;

    /**
     * Constructor, takes in a cursor object which operates as the basis of data
     * collection to show.
     *
     * @param context  used to get the string resources
     * @param cursor   the query containing a list of data to show
     * @param listener to measure mouse clicks to pass back to the containing fragment
     */
    public SavedLocationAdapter(Context context, Cursor cursor, SavedLocationViewHolder.SavedLocationListClickListener listener) {
        super(cursor);
        mListener = listener;
    }

    @Override
    public SavedLocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_saved_location_row, parent, false);
        SavedLocationViewHolder vh = new SavedLocationViewHolder(parent.getContext(), itemView, mListener);
        return vh;
    }

    /**
     * Updates the view holder with new data, and also ensures the location
     * as the appropriate icon loaded for its type.
     *
     * @param viewHolder the view to update
     * @param cursor     the cursor object to get the current location object
     */
    @Override
    public void onBindViewHolder(SavedLocationViewHolder viewHolder, Cursor cursor) {
        // Get the location to show via the cursor data
        Location location = LocationRepo.fromCursor(cursor);
        location.setImageIcon(LocationTypeHelper.getIcon(location.getType()));
        viewHolder.updateEntries(location);
    }
}
