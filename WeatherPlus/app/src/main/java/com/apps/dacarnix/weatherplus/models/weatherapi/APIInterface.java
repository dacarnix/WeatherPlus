package com.apps.dacarnix.weatherplus.models.weatherapi;

import com.apps.dacarnix.weatherplus.models.weatherapi.data.WeatherResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * An interface which defines the URL used in the API calls
 * to the openweathermap. Currently only uses one API
 * call which gets the hourly weather data at a long/lat location.
 */
public interface APIInterface {

    @GET(APIUtils.WEATHER_HOURLY_API)
    Call<WeatherResponse> getWeatherAtLocation(@QueryMap Map<String, String> options);

}