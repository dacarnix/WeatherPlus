package com.apps.dacarnix.weatherplus.models.recyclerview;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.dacarnix.weatherplus.R;
import com.apps.dacarnix.weatherplus.models.Location;

/**
 * The viewholder for the rows in the recyclerview that shows
 * saved locations to the user.
 */
public class SavedLocationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, MenuItem.OnMenuItemClickListener {

    private final SavedLocationListClickListener mListener;

    private TextView mTextName;
    private TextView mTextCountry;
    private ImageView mTypeIcon;
    private PopupMenu mMenu;

    @Nullable
    private Location mLocation;

    /**
     * Gets references to the appropriate UI components, inflates a menu to place
     * under a custom PopupMenu, and then sets the listeners.
     *
     * @param context  to inflate the menu
     * @param itemView the itemview containing the UI
     * @param listener to record onClicks
     */
    public SavedLocationViewHolder(Context context, View itemView, SavedLocationListClickListener listener) {
        super(itemView);
        mListener = listener;

        mTextName = (TextView) itemView.findViewById(R.id.text_name);
        mTextCountry = (TextView) itemView.findViewById(R.id.text_country);
        mTypeIcon = (ImageView) itemView.findViewById(R.id.image_item_type);
        ImageView mOverflow = (ImageView) itemView.findViewById(R.id.image_overflow);

        mMenu = new PopupMenu(context, mOverflow);
        mMenu.getMenuInflater().inflate(R.menu.item_saved_location, mMenu.getMenu());

        itemView.setOnClickListener(this);
        mOverflow.setOnClickListener(this);

        // Was unable to get the mMenu.setOnMenuItemClickListener(this); working,
        // and therefore used a work around by adding listeners to the items directly.
        mMenu.getMenu().findItem(R.id.menu_delete).setOnMenuItemClickListener(this);
        mMenu.getMenu().findItem(R.id.menu_share).setOnMenuItemClickListener(this);
    }

    /**
     * Updates the UI components with values associated with a new location entry
     *
     * @param location the entry
     */
    public void updateEntries(Location location) {
        mTextName.setText(location.getName());
        mTextCountry.setText(location.getCountry());
        mTypeIcon.setBackgroundResource(location.getImageIcon());
        mLocation = location;
    }

    /**
     * Handles the onclick event for each row.
     *
     * @param v the view clicked on
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_overflow:
                mMenu.show();
                break;
            default:
                // They just clicked layout
                mListener.onClick(mLocation, SavedLocationItemClickAction.GoToWeather);
                break;
        }
    }

    /**
     * Checks which of the overflow buttons was pressed and triggers the event appropriately
     * that will call a method in the fragment containing this.
     *
     * @param item the item clickedon
     * @return true !
     */
    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_delete:
                mListener.onClick(mLocation, SavedLocationItemClickAction.Delete);
                break;
            case R.id.menu_share:
                mListener.onClick(mLocation, SavedLocationItemClickAction.Share);
                break;
        }
        return true;
    }

    /**
     * A simple interface to create a valid on-click event within the saved location recyclerview
     */
    public interface SavedLocationListClickListener {
        void onClick(Location location, SavedLocationItemClickAction action);
    }
}
