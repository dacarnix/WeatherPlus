package com.apps.dacarnix.weatherplus.database.model;

import android.provider.BaseColumns;

/**
 * Contains the contract for the Location table.
 * All column names are present here
 */
public final class LocationContract {
    private LocationContract() {}

    public static class LocationEntry implements BaseColumns {
        public static final String TABLE_NAME = "location";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_LONG = "long";
        public static final String COLUMN_NAME_LAT = "lat";
        public static final String COLUMN_NAME_COUNTRY = "country";
        public static final String COLUMN_NAME_TIMESTAMP = "ts";
        public static final String COLUMN_NAME_TYPE = "type";
    }
}