package com.apps.dacarnix.weatherplus.helpers;

/**
 * Holds constants related to map activity to help abstract
 * out code and create better seperate of concerns.
 * <p>
 * Improves overall code quality
 */
public class MapActivityConstants {
    // Holds information regarding the marker selected, for persistence on rotation
    public static final String BUNDLE_SELECTED_MARKER_LAT = "BUNDLE_SELECTED_MARKER_LAT";
    public static final String BUNDLE_SELECTED_MARKER_LONG = "BUNDLE_SELECTED_MARKER_LONG";
    public static final String BUNDLE_SELECTED_MARKER_INDEX = "BUNDLE_SELECTED_MARKER_INDEX";
    public static final String BUNDLE_SELECTED_MARKER_SELECTED = "BUNDLE_SELECTED_MARKER_SELECTED";

    // Indicating if the settings were down / open on rotation
    public static final String BUNDLE_SETTINGS_ACTIVE = "BUNDLE_SETTINGS_ACTIVE";

    // One hour is the limit for weather data before it must be refreshed
    public static final int WEATHER_DATA_TIMEOUT = 60 * 60;

    // A unique code to represent the create location activity
    public static final int UNIQUE_CODE_CREATE_LOCATION = 1234;

    // A unique code to represent the show location activity
    public static final int UNIQUE_CODE_LOCATION = 1235;

    // Used to identify if the marker was saved or not on rotation
    public static final int ON_TAP_MARKER_INDEX = -1;
    public static final int NO_MARKER_SELECTED = -2;

    // Unique code identifiers for the permissions required for this activity
    public static final int PERMISSIONS_FINE_LOCATION_CODE = 1234;
    //Every 5 seconds is fine
    public static final int MINIMUM_LOCATION_REFRESH_TIME = 500;
    public static final int MINIMUM_LOCATION_DISTANCE = 1;

    // Used for accessing the preferences in the prefencescreen UI
    // The keys match the keys present in xml/preference_screen
    public static final String SHOW_LOCATIONS_AS_MARKERS = "show_locations_as_markers";
    public static final String FITLER_LOCATION_MARKERS = "show_marker_filter";
    public static final String FILTER_ALL_WILDCARD = "All";

    // Used in moving the camera around the map
    public static final int DEFAULT_ZOOM_LEVEL = 15;
    public static final int BROAD_ZOOM_LEVEL = 10;

    // Lat, long, country
    public static final String LOCATION_SHARING_PREFIX = "LOCATION: %f %f";
    public static final String DEFAULT_COUNTRY = "No country found";
}
