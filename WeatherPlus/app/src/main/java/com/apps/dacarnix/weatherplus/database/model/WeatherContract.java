package com.apps.dacarnix.weatherplus.database.model;

import android.provider.BaseColumns;

/**
 * Contains the contract for the weather data table.
 * All column names are present here
 */
public final class WeatherContract {
    private WeatherContract() {
    }

    /**
     * Each weather entry is associated with a saved location, it should be a 1 for 1 deal,
     * which saves the most recent hours with of weather for a given location, such that
     * the API does not have to keep being re-called.
     */
    public static class WeatherEntry implements BaseColumns {
        public static final String TABLE_NAME = "weather";
        // The ID of the saved location row this is attached to
        public static final String COLUMN_NAME_LOC_ID = "locid";
        public static final String COLUMN_NAME_TEMP = "temp";
        public static final String COLUMN_NAME_HUMIDITY = "hum";
        public static final String COLUMN_NAME_WEATHER_DESCRIPTION = "desc";
        public static final String COLUMN_NAME_WEATHER_ICON = "icon";
        public static final String COLUMN_NAME_TIMESTAMP = "ts";
    }
}