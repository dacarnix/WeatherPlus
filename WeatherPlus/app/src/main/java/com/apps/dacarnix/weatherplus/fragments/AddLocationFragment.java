package com.apps.dacarnix.weatherplus.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.apps.dacarnix.weatherplus.R;
import com.apps.dacarnix.weatherplus.activities.AddLocationActivity;
import com.apps.dacarnix.weatherplus.database.DatabaseTimestampHelper;
import com.apps.dacarnix.weatherplus.models.Location;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * A basic fragment holding the UI components
 * for getting information regarding a new location.
 * <p>
 * Currently only held within AddLocationActivity
 */
public class AddLocationFragment extends Fragment implements View.OnClickListener {

    private AddLocationFragmentListener mListener;

    private FloatingActionButton mAcceptFab;
    private EditText mNameField;
    private Spinner mSpinnerType;

    private TextView mTextLat;
    private TextView mTextLong;
    private TextView mTextCountry;

    private Double mLong;
    private Double mLat;
    private String mCountry;

    public AddLocationFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_location, container, false);
        mAcceptFab = (FloatingActionButton) view.findViewById(R.id.fab_accept_location);
        mAcceptFab.setOnClickListener(this);
        mNameField = (EditText) view.findViewById(R.id.edit_name);
        mNameField.addTextChangedListener(getEditTextWatcher());
        setUpSpinner(view);
        mTextLat = (TextView) view.findViewById(R.id.text_lat_value);
        mTextLong = (TextView) view.findViewById(R.id.text_long_value);
        mTextCountry = (TextView) view.findViewById(R.id.text_country_value);

        return view;
    }

    /**
     * Saves the data contained in the fragment so that
     * it remains persistent on state changes (such as a rotation).
     *
     * @param view               of the fragment
     * @param savedInstanceState where to save the data
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState != null) {
            if (mCountry != null) mTextLat.setText(mCountry);
            if (mLat != null) mTextLat.setText(mLat.toString());
            if (mLong != null) mTextLong.setText(mLong.toString());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Retain this fragment on screen rotations
        setRetainInstance(true);
    }

    /**
     * Get the context of the context the fragment is contained in
     * to send data to when clicked on and appropriate.
     *
     * @param context the context contained in
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AddLocationFragmentListener) {
            mListener = (AddLocationFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement AddLocationFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * Sets the contents of widgets in the fragment
     *
     * @param country the country
     * @param lon     the longitude
     * @param lat     the latitude
     */
    public void updateContent(String country, Double lon, Double lat) {
        mCountry = country;
        mLong = lon;
        mLat = lat;
        NumberFormat formatter = new DecimalFormat("#0.00");
        mTextCountry.setText(mCountry);
        mTextLat.setText(formatter.format(lat));
        mTextLong.setText(formatter.format(lon));
        checkDataFinished();
    }

    /**
     * Sets up the spinner object with the type values
     *
     * @param view the view containing the spinner
     */
    private void setUpSpinner(View view) {
        mSpinnerType = (Spinner) view.findViewById(R.id.spinner_type);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.location_types, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerType.setAdapter(adapter);
    }

    /**
     * Gets the class used to listen to changes in the EditText.
     * This way we can check after each change if all the data has been
     * filled in.
     *
     * @return a text watcher
     */
    @NonNull
    private TextWatcher getEditTextWatcher() {
        return new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                checkDataFinished();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        };
    }

    /**
     * Checks to ensure a name has been entered before allowing the creation of a location
     * and that the country has been determined
     */
    private void checkDataFinished() {
        mAcceptFab.setVisibility(!mNameField.getText().toString()
                .isEmpty() &&
                !mTextCountry.getText().toString().equals(AddLocationActivity.FINDING_COUNTRY_STRING)
                ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_accept_location:
                mListener.onLocationClick(new Location(
                        mSpinnerType.getSelectedItem().toString(),
                        mNameField.getText().toString(),
                        mLong,
                        mLat,
                        mCountry,
                        DatabaseTimestampHelper.getCurrentUnixTime()
                ));
                break;
        }
    }

    /**
     * An interface to pass down to the activity which contains it information
     */
    public interface AddLocationFragmentListener {
        void onLocationClick(Location toAdd);
    }

}
