package com.apps.dacarnix.weatherplus.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v14.preference.PreferenceFragment;
import android.support.v7.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps.dacarnix.weatherplus.R;

/**
 * A preference fragment that holds additional settings to toggle
 * for the users map. Contains code related to handling changes to the preferences.
 */
public class MapPreferenceFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    private PreferenceChangeListener mListener;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preference_fragment_map);
        PreferenceManager.getDefaultSharedPreferences(getActivity()).registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        view.setBackgroundColor(Color.WHITE);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * Get the context of the context the fragment is contained in
     * to send data to when clicked on and appropriate.
     *
     * @param context the context contained in
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof PreferenceChangeListener) {
            mListener = (PreferenceChangeListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement PreferenceChangeListener");
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        mListener.onPreferenceChange();
    }

    /**
     * Used to communicate with the mapactivity
     */
    public interface PreferenceChangeListener {
        void onPreferenceChange();
    }
}