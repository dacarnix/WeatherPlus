package com.apps.dacarnix.weatherplus.models.weatherapi.data;

import java.util.List;

import com.apps.dacarnix.weatherplus.models.WeatherData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * The main class which holds the majority of JSON children in the weather response
 * from openweathermap. Holds weather description and temperature data.
 */
public class WeatherResponse {

    @SerializedName("weather")
    @Expose
    private List<Weather> mWeather;

    @SerializedName("main")
    @Expose
    private Temperature mTemperature;

    @SerializedName("dt")
    @Expose
    private Long mUnixTimeStamp;

    public Weather getWeather() {
        return mWeather.get(0);
    }

    public Temperature getTemperature() {
        return mTemperature;
    }

    /**
     * Converts the broken up java objects from GSON in to our
     * custom weather data class.
     *
     * @return a usable weather data class
     */
    public WeatherData toWeatherData() {
        return new WeatherData(mTemperature.getTemp(),
                mTemperature.getHumidity(),
                getWeather().getDescription(),
                getWeather().getIcon(),
                mUnixTimeStamp);
    }
}