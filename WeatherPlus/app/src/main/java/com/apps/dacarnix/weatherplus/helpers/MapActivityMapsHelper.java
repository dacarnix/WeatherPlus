package com.apps.dacarnix.weatherplus.helpers;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.apps.dacarnix.weatherplus.models.Location;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

/**
 * A class which contains static methods to help use the
 * google maps API for location information. This class is exclusively used
 * inside the MapsActivity and MyMapFragment class.
 */

public class MapActivityMapsHelper {
    private static final String TAG = MapActivityMapsHelper.class.getSimpleName();
    public static final String CLICKED_LOCATION_STRING = "You clicked here";
    private static final float NON_SAVED_MARKER_COLOUR = BitmapDescriptorFactory.HUE_AZURE;
    private static final float SAVED_MARKER_COLOUR = BitmapDescriptorFactory.HUE_RED;

    /**
     * Attempts to find the best address suited to a given string.
     *
     * @param context  the context of the activity calling it
     * @param location the location to find the country around
     * @return the location best suited to the address (may be null)
     */
    @Nullable
    public static LatLng getBestAddress(Context context, String location) {
        List<Address> addressList;
        Address address;
        Geocoder geocoder = new Geocoder(context);
        try {
            addressList = geocoder.getFromLocationName(location, 1);
            if (addressList.size() > 0) {
                address = addressList.get(0);
                return new LatLng(address.getLatitude(), address.getLongitude());
            }
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
        return null;
    }

    /**
     * Returns a new marker to add to the map
     *
     * @param location to create the marker at
     * @return the marker to add
     */
    public static MarkerOptions createNewMarker(LatLng location) {
        return new MarkerOptions()
                .position(location)
                .title(CLICKED_LOCATION_STRING)
                .icon(BitmapDescriptorFactory.defaultMarker(NON_SAVED_MARKER_COLOUR));
    }


    /**
     * Returns a new marker to add to the map based on a saved location
     *
     * @param location to create the marker at
     * @return the marker to add
     */
    public static MarkerOptions createSavedMarker(Location location) {
        return new MarkerOptions()
                .position(new LatLng(location.getLatitude(), location.getLongitude()))
                .title(location.getName())
                .icon(BitmapDescriptorFactory.defaultMarker(SAVED_MARKER_COLOUR));
    }

    /**
     * Returns the criteria to use in this application
     *
     * @return relevant criteria
     */
    public static Criteria getCriteria() {
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setSpeedRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_HIGH);
        return criteria;
    }

    /**
     * Checks to see if the intent that started main activity
     * contains any string that we could pass in to the google maps
     *
     * @param intent the intent that started main activity
     * @return a string if any, else null
     */
    @Nullable
    public static String getIntentString(Intent intent) {
        if (intent != null) {
            String action = intent.getAction();
            String type = intent.getType();
            if (Intent.ACTION_SEND.equals(action) && type != null) {
                if ("text/plain".equals(type)) {
                    return intent.getStringExtra(Intent.EXTRA_TEXT);
                }
            }
        }
        return null;
    }
}
