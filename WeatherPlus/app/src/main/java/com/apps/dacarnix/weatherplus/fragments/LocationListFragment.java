package com.apps.dacarnix.weatherplus.fragments;


import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apps.dacarnix.weatherplus.R;
import com.apps.dacarnix.weatherplus.database.repo.LocationRepo;
import com.apps.dacarnix.weatherplus.models.Location;
import com.apps.dacarnix.weatherplus.models.recyclerview.SavedLocationAdapter;
import com.apps.dacarnix.weatherplus.models.recyclerview.SavedLocationItemClickAction;
import com.apps.dacarnix.weatherplus.models.recyclerview.SavedLocationViewHolder;

/**
 * A fragment which contains a list of all the saved locations.
 */
public class LocationListFragment extends Fragment {

    @Nullable
    private LocationListFragmentListener mListener;
    @Nullable
    private RecyclerView mEntriesList;
    @Nullable
    private SavedLocationAdapter mRecyclerAdapter;
    @Nullable
    private TextView mEmptyText;

    /**
     * Creates the fragment, and sets the appropriate variables to
     * references in the UI.
     * <p>
     * Sets up the cursor and loads the appropriate initial data
     * to the saved locations list view
     *
     * @param inflater           used to inflate the layout
     * @param container          the container to hold the fragment
     * @param savedInstanceState holds potentially saved information
     * @return the view of this fragment
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_location_list, container, false);

        mEntriesList = (RecyclerView) v.findViewById(R.id.recycler_view_entries);
        mEmptyText = (TextView) v.findViewById(R.id.text_empty_view);

        refresh();

        return v;
    }

    /**
     * Get the context of the context the fragment is contained in
     * to send data to when clicked on and appropriate.
     *
     * @param context the context contained in
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof LocationListFragmentListener) {
            mListener = (LocationListFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement LocationListFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * When the cursor should be updated (from a UI change)
     * it will check if a filter query was present and update
     * it accordingly.
     *
     * @param query the potential query to use in the cursor filter
     */
    public void updateCursor(String query) {
        Cursor c;
        if (query == null) {
            c = LocationRepo.getAllRows();
        } else {
            c = LocationRepo.getRowsThatContain(query);
        }
        mRecyclerAdapter.changeCursor(c);
        checkContent();
    }

    /**
     * Refreshes the lists data
     */
    public void refresh() {
        Cursor cursor = LocationRepo.getAllRows();
        SavedLocationViewHolder.SavedLocationListClickListener listener = new SavedLocationViewHolder.SavedLocationListClickListener() {
            @Override
            public void onClick(Location location, SavedLocationItemClickAction action) {
                mListener.onLocationClick(location, action);
            }
        };
        mRecyclerAdapter = new SavedLocationAdapter(getContext(), cursor, listener);
        mEntriesList.setAdapter(mRecyclerAdapter);

        RecyclerView.LayoutManager layout = new LinearLayoutManager(getActivity());
        mEntriesList.setLayoutManager(layout);

        // Add a line separator between each row
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(
                mEntriesList.getContext(),
                DividerItemDecoration.VERTICAL);
        mEntriesList.addItemDecoration(mDividerItemDecoration);
        checkContent();
    }

    /**
     * Checks to see if any data is present to show.
     * If not, show appropriate text.
     */
    private void checkContent() {
        if (mRecyclerAdapter.getItemCount() == 0) {
            mEntriesList.setVisibility(View.GONE);
            mEmptyText.setVisibility(View.VISIBLE);
        } else {
            mEntriesList.setVisibility(View.VISIBLE);
            mEmptyText.setVisibility(View.GONE);
        }
    }

    /**
     * An interface to pass down to the activity which contains it information
     */
    public interface LocationListFragmentListener {
        void onLocationClick(Location location, SavedLocationItemClickAction action);
    }

}
