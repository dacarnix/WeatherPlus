package com.apps.dacarnix.weatherplus.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.dacarnix.weatherplus.R;
import com.apps.dacarnix.weatherplus.database.DatabaseTimestampHelper;
import com.apps.dacarnix.weatherplus.models.Location;
import com.apps.dacarnix.weatherplus.models.WeatherData;

/**
 * A fragment used which will display more indepth information regarding a saved location.
 * This will include the lat, long coordinates and if available (due to network connections),
 * it should show hourly weather information.
 */
public class SeeLocationFragment extends Fragment {

    // Location attributes
    private TextView mTextTitle;
    private TextView mTextType;
    private TextView mTextCountry;

    // Weather attributes
    private ImageView mWeatherIcon;
    private TextView mWeatherTemp;
    private TextView mWeatherHumidity;
    private TextView mWeatherDescription;
    private TextView mLastWeatherUpdate;
    private View mWeatherContainer;

    private TextView mNoWeatherText;

    public SeeLocationFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_see_location, container, false);
        loadUi(v);
        return v;
    }

    /**
     * Loads the respective UI components from the load to manipulate
     *
     * @param view the fragment view
     */
    private void loadUi(View view) {
        mTextTitle = (TextView) view.findViewById(R.id.text_title);
        mTextType = (TextView) view.findViewById(R.id.text_type);
        mTextCountry = (TextView) view.findViewById(R.id.text_country);

        mNoWeatherText = (TextView) view.findViewById(R.id.text_no_weather_data);
        mWeatherContainer = view.findViewById(R.id.weather_info_container);

        mWeatherIcon = (ImageView) mWeatherContainer.findViewById(R.id.image_weather_icon);
        mWeatherTemp = (TextView) mWeatherContainer.findViewById(R.id.text_temp);
        mWeatherHumidity = (TextView) mWeatherContainer.findViewById(R.id.text_hum);
        mWeatherDescription = (TextView) mWeatherContainer.findViewById(R.id.text_description);
        mLastWeatherUpdate = (TextView) mWeatherContainer.findViewById(R.id.text_last_update);

        // These are usually invisible, make them visible.
        mWeatherDescription.setVisibility(View.VISIBLE);
        mWeatherContainer.findViewById(R.id.text_description_title).setVisibility(View.VISIBLE);
    }

    /**
     * Sets up the UI with the appropriate contents based on the location and weather data provided
     *
     * @param location     the location
     * @param weather      the weather data associated with the location
     * @param masterDetail whether this is a master detail or in an activity
     */
    public void loadContents(Location location, @Nullable WeatherData weather, boolean masterDetail) {
        mTextTitle.setText(location.getName());
        mTextType.setText(location.getType());
        mTextCountry.setText(location.getCountry());

        if (masterDetail) {
            // If it is a master detail, no need to show country as it is already on the screen
            mTextCountry.setVisibility(View.GONE);
        } else {
            mTextCountry.setVisibility(View.VISIBLE);
        }

        // Weather may be null
        if (weather != null) {
            mWeatherIcon.setBackgroundResource(weather.getIconResourceId(getContext()));
            mWeatherTemp.setText(weather.getTempInCelcius());
            mWeatherHumidity.setText(weather.getHumidityAsString());

            String description = weather.getDescription();
            description = Character.toUpperCase(description.charAt(0)) + description.substring(1);
            // Make the first letter upper case
            mWeatherDescription.setText(description);

            long timeNow = DatabaseTimestampHelper.getCurrentUnixTime();

            //Find difference
            int difference = (int) (timeNow - weather.getUnixTimeOfData());
            mLastWeatherUpdate.setText(convertSecondsToReadable(difference));

        } else {
            // Set default data as it is null weather!
            // This may occur if there is a network issue.
            mNoWeatherText.setVisibility(View.VISIBLE);
            mWeatherContainer.setVisibility(View.GONE);
        }

    }

    /**
     * Converts seconds in to a human readable format.
     * Used to show the difference in time since the last weather update.
     *
     * @param seconds the difference in seconds
     * @return a string to show the user
     */
    private String convertSecondsToReadable(int seconds) {
        int hours = seconds / 3600;
        int minutes = (seconds % 3600) / 60;
        int realSeconds = seconds % 60;
        return String.format("%02dh%02dm%02ds ago", hours, minutes, realSeconds);
    }

}
