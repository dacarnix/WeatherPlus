package com.apps.dacarnix.weatherplus.models.recyclerview;

import android.database.Cursor;
import android.database.DataSetObserver;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;

/**
 * An abstract class which transforms the basic view holder for the recyclerview
 * to be able to pull data from a cursor (a query object) rather than just from
 * a list of data. This will be used extensively in the Saved Locations recycler list
 * (seen in Locations Activity) to pull data from a database directly.
 * <p>
 * Code was heavily influenced and based off code found at https://gist.github.com/tuanchauict/c6c1eda617523de224c5
 * which is licensed under Apache License, Version 2.0.
 *
 * @param <VH> a generic view holder
 */
public abstract class CursorRecyclerViewAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    @Nullable
    private Cursor mCursor;
    private boolean mDataValid;
    private int mRowIdColumn;
    @Nullable
    private DataSetObserver mDataSetObserver;

    /**
     * Takes in a cursor as the core of the data source.
     * Ensures that each row has a unique ID, and is a valid cursor
     *
     * @param cursor the cursor object to base the data off of
     */
    public CursorRecyclerViewAdapter(@Nullable Cursor cursor) {
        mCursor = cursor;
        mDataValid = cursor != null;
        mRowIdColumn = mDataValid ? mCursor.getColumnIndex("_id") : -1;
        mDataSetObserver = new NotifyingDataSetObserver();
        if (mCursor != null) {
            mCursor.registerDataSetObserver(mDataSetObserver);
        }
    }

    @Override
    public int getItemCount() {
        if (mDataValid && mCursor != null) {
            return mCursor.getCount();
        }
        return 0;
    }

    @Override
    public long getItemId(int position) {
        if (mDataValid && mCursor != null && mCursor.moveToPosition(position)) {
            return mCursor.getLong(mRowIdColumn);
        }
        return 0;
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(true);
    }

    public abstract void onBindViewHolder(VH viewHolder, Cursor cursor);

    /**
     * Ensure that the cursor is set up correctly, and not pointing to invalid data
     *
     * @param viewHolder the viewholder
     * @param position   the position to go to
     */
    @Override
    public void onBindViewHolder(VH viewHolder, int position) {
        if (!mDataValid) {
            throw new IllegalStateException("Cursor must point to valid data");
        }
        if (!mCursor.moveToPosition(position)) {
            throw new IllegalStateException("Couldn't move cursor to position " + position);
        }
        onBindViewHolder(viewHolder, mCursor);
    }

    /**
     * Change the underlying cursor to a new cursor. If there is an existing cursor it will be
     * closed.
     *
     * @param cursor the cursor to change to
     */
    public void changeCursor(Cursor cursor) {
        Cursor old = swapCursor(cursor);
        if (old != null) {
            old.close();
        }
    }

    /**
     * Swaps the cursor used as the underlying data source to a new one
     *
     * @param newCursor the cursor to change to
     * @return the old cursor, if needed
     */
    public Cursor swapCursor(Cursor newCursor) {
        if (newCursor == mCursor) {
            return null;
        }
        final Cursor oldCursor = mCursor;
        if (oldCursor != null && mDataSetObserver != null) {
            oldCursor.unregisterDataSetObserver(mDataSetObserver);
        }
        mCursor = newCursor;
        if (mCursor != null) {
            if (mDataSetObserver != null) {
                mCursor.registerDataSetObserver(mDataSetObserver);
            }
            mRowIdColumn = newCursor.getColumnIndexOrThrow("_id");
            mDataValid = true;
            notifyDataSetChanged();
        } else {
            mRowIdColumn = -1;
            mDataValid = false;
            notifyDataSetChanged();
        }
        return oldCursor;
    }

    private class NotifyingDataSetObserver extends DataSetObserver {
        @Override
        public void onChanged() {
            super.onChanged();
            mDataValid = true;
            notifyDataSetChanged();
        }

        @Override
        public void onInvalidated() {
            super.onInvalidated();
            mDataValid = false;
            notifyDataSetChanged();
        }
    }
}